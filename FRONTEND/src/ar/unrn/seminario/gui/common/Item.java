package ar.unrn.seminario.gui.common;

public class Item<T> {
	private T item;
	private String nombreAMostrar;
	public Item(T item, String nombreAMostrar) {
		this.item = item;
		this.nombreAMostrar = nombreAMostrar;
	}
	public T getItem() {
		return item;
	}
	public String getNombreAMostrar() {
		return nombreAMostrar;
	}
	
	public String toString() {
		return getNombreAMostrar();
	}
}
