package ar.unrn.seminario.gui.common;

import java.awt.Component;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JOptionPane;

public class Helpers {

	public static LocalDate dateALocalDate(Date date) {
		Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault(); // O puedes usar la zona horaria que desees
        return instant.atZone(zoneId).toLocalDate();
	}
	
	public static void mostrarMensajeDeError(Component parentComponent, Throwable causa, String titulo) {
		// Arma el mensaje incluyendo más detalles.
		StringBuffer sb = new StringBuffer(causa.getMessage());
		causa = causa.getCause();
		if(causa != null) sb.append("\n");
		while(causa != null) {
			sb.append("\n");
			sb.append(causa.toString());
			causa = causa.getCause();
		}
		// Muestra el mensaje de error en un cuadro si la ventana es visible.
		if(parentComponent.isVisible())
			JOptionPane.showMessageDialog(parentComponent, sb, titulo, JOptionPane.ERROR_MESSAGE);
		else
			System.err.println(sb);
	}
}
