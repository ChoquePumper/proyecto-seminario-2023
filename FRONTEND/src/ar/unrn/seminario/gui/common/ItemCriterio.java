package ar.unrn.seminario.gui.common;

import java.util.function.Function;

import ar.edu.unrn.seminario.api.Filtro;

public class ItemCriterio {
	private String nombre;
	private Function<Object,Filtro> filtro;
	public ItemCriterio(String nombre, Function<Object,Filtro> filtro) {
		this.nombre = nombre;
		this.filtro = filtro;
	}
	public Filtro getFiltro(Object o) {
		return filtro.apply(o);
	}
	public String toString() {
		return nombre;
	}
}
