package ar.unrn.seminario.gui.common;

import ar.edu.unrn.seminario.api.Ordenado;

public class ItemOrdenado {
	private String campo;
	private String descripcion;
	
	public ItemOrdenado(String campo, String descripcion) {
		super();
		this.campo = campo;
		this.descripcion = descripcion;
	}
	public String getCampo() {
		return campo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public Ordenado getOrdenado(boolean invertido) {
		if(campo==null)
			return null;
		return new Ordenado(campo,invertido);
	}
	public String toString() {
		return getDescripcion();
	}
}
