package ar.unrn.seminario.gui.campana;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.unrn.seminario.gui.common.Helpers;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JScrollPane;
import com.toedter.calendar.JDateChooser;

public class AltaCampana extends JDialog {

	protected IApi api;
	protected String tituloMsjError;
	
	private JPanel contentPane, panelBotones;
	protected JTextField txtNombre;
	protected JDateChooser txtFechaEdicion;
	private JLabel lblNombre, lblFecha, lblDescripcion;
	protected JButton btnCrear, btnCancelar;
	protected JTextArea txtDescripcion;
	private JScrollPane scrollPane;
	
	/**
	 * Create the frame.
	 */
	public AltaCampana(IApi api) {
		this.api = api;
		tituloMsjError = "Error al registrar campaña";
		setTitle("                          Crear Campaña");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 430, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("Nombre: ");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(29, 28, 102, 20);
		contentPane.add(lblNombre);
		
		lblFecha = new JLabel("Fecha Edicion:");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setBounds(29, 72, 121, 29);
		contentPane.add(lblFecha);
		
		lblDescripcion = new JLabel("Descripcion: ");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDescripcion.setBounds(29, 114, 102, 20);
		contentPane.add(lblDescripcion);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(159, 28, 230, 21);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		
		txtFechaEdicion = new JDateChooser();
		txtFechaEdicion.setBounds(159, 73, 177, 29);
		contentPane.add(txtFechaEdicion);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(159, 113, 230, 52);
		contentPane.add(scrollPane);
		
		txtDescripcion = new JTextArea();
		scrollPane.setViewportView(txtDescripcion);
		
		panelBotones = new JPanel();
		panelBotones.setBackground(new Color(128, 255, 128));
		panelBotones.setBounds(10, 173, 394, 52);
		contentPane.add(panelBotones);
		panelBotones.setLayout(null);
		
		btnCrear = new JButton("Crear");
		btnCrear.setBounds(196, 11, 89, 30);
		panelBotones.add(btnCrear);
		
		btnCrear.addActionListener(getAccion());
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(295, 11, 89, 30);
		panelBotones.add(btnCancelar);
		
		btnCancelar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});
		
		
	}
	
	protected ActionListener getAccion() {
		return new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(enviar()) dispose();
			}
			
		};
	}
	
	protected boolean enviar() {
		boolean exito = false;
		try {
			// Preparar datos
			String descripcion = txtDescripcion.getText();
			String nombre = txtNombre.getText();
//			String fechaTex = txtFechaEdicion.getText()+"/"+ txtMes.getText()+"/"+  txtAnio.getText();
//
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//			LocalDate fecha = LocalDate.parse(fechaTex,formatter);
			Date dateFecha = txtFechaEdicion.getDate();
			if (dateFecha == null)
				throw new AppException("La fecha no tiene un valor válido.");
			LocalDate fecha = Helpers.dateALocalDate(dateFecha);

			exito = registrar(fecha, nombre, descripcion);
		} catch (AppException e) {
			onFalloEnvio(e);
		}
		if(exito) onEnvioExitoso();
		return exito;
	}
	
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Campaña registrada con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	// Esto lo cree por si la ventana de modificar lo maneja de forma diferente.
	protected void onFalloEnvio(AppException excepcion) {
		try {
			throw excepcion;
		} catch (DataEmptyException e) {
			JOptionPane.showMessageDialog(this, String.format("El campo '%s' está vacío", e.getNombreCampo()), tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DuplicateException e) {
			JOptionPane.showMessageDialog(this, "Ya existe una campaña con el mismo nombre.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DataAccessException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "No se pudo acceder a los datos.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (AppException e) {
			e.printStackTrace();
			//JOptionPane.showMessageDialog(null, e.getMessage(), "Error "+e.getClass().getName(), JOptionPane.ERROR_MESSAGE);
			Helpers.mostrarMensajeDeError(this, e, tituloMsjError);
			//JOptionPane.showMessageDialog(this, "Error al registrar la campaña: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	protected boolean registrar(LocalDate fecha, String nombre, String descripcion) {
		api.registrarCampana(null, fecha, nombre, descripcion);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}
}
