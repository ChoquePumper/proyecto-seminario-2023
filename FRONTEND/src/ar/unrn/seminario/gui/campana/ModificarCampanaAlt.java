package ar.unrn.seminario.gui.campana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.text.DateFormatter;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.unrn.seminario.gui.common.Helpers;

public class ModificarCampanaAlt extends AltaCampana {
	private boolean resultado = false;
	private CampanaDTO campana;
	public ModificarCampanaAlt(IApi api, Integer codigoCampana) {
		super(api);

		tituloMsjError = "Error al modificar la campaña";
		this.campana = api.obtenerCampanaPorCodigo(codigoCampana);
		
		// Retocar la GUI.
		setTitle("                     Modificar Campaña");
		btnCrear.setText("Modificar");

		txtNombre.setText(campana.getNombre());
		txtDescripcion.setText(campana.getDescripcion());
		LocalDate ld = LocalDate.parse(campana.getFechaEdicion(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		//txtFechaEdicion.setDate( new Date(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth()) );
		txtFechaEdicion.setDate( Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant()) );
	}
	
	public boolean mostrar() {
		this.setModal(true);
		this.setVisible(true);
		return resultado;
	}
	
	@Override
	protected boolean enviar() {
		resultado = super.enviar();
		return resultado;
	}
	
	@Override
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Campaña modificada con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	@Override
	protected boolean registrar(LocalDate fecha, String nombre, String descripcion) {
		api.modificarCampana(campana.getCodigo(), fecha, nombre, descripcion);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}

}
