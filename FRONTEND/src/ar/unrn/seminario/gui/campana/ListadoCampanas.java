package ar.unrn.seminario.gui.campana;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ar.edu.unrn.seminario.api.Filtro;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.Ordenado;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.unrn.seminario.gui.common.Helpers;
import ar.unrn.seminario.gui.common.Item;
import ar.unrn.seminario.gui.common.ItemCriterio;
import ar.unrn.seminario.gui.common.ItemOrdenado;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;

import javax.swing.SpringLayout;
import javax.swing.SwingWorker;

import java.awt.BorderLayout;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JProgressBar;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.border.CompoundBorder;
import javax.swing.Box;
import javax.swing.JLabel;
import java.awt.Insets;

public class ListadoCampanas extends JFrame {

	private IApi api;
	private JPanel contentPane, panelBotones;
	private DefaultTableModel tblmodel;
	private JScrollPane scrollPane;
	private JButton btnModificar, btnEliminar, btnEspAsociados, btnVolver;
	private JTable table;
	private JProgressBar progressBar;
	private RecuperarListaWorker workerRecuperarLista;
	
	private String[] titulos = {"Codigo","Nombre","Fecha Edicion","Descripcion"};
	private Component verticalStrut;
	private Component verticalGlue;
	private Box horizontalBox;
	private Component horizontalGlue;
	private JScrollPane scrollPane_1;
	private JTable tableFiltros;
	private Component verticalStrut_1;
	private Component verticalStrut_2;
	private Component verticalStrut_3;

	private ItemCriterio[] filtrosTexto = {
			new ItemCriterio("es", (o) -> Filtro.es(o)),
			new ItemCriterio("contiene", (o) -> Filtro.contiene((String)o)),
			new ItemCriterio("empieza con", (o) -> Filtro.comienzaCon((String)o)),
			new ItemCriterio("termina con", (o) -> Filtro.terminaCon((String)o))
	};
	private Map<String, Item<String>> camposParaFiltrar = Map.of(
			"nombre", new Item<String>("nombre", "Nombre"),
			"descripcion", new Item<String>("descripcion", "Descripción")
			);
	
	private ItemOrdenado[] camposParaOrdenar = {
			new ItemOrdenado(null, "(sin ordenar)"),
			new ItemOrdenado("nombre", "Nombre"),
			new ItemOrdenado("codigo", "Código"),
			new ItemOrdenado("fechaEdicion", "Fecha edición"),
	};
	private final String keyDireccionOrdenadoInvertido = "Ordenado.invertido";
	
	private JButton btnFiltrar;
	private Box horizontalBox_1;
	private JComboBox<ItemOrdenado> cbOrdenarPor;
	private JLabel lblOrdenarPor;
	private JButton btnOrdenAscDesc;
	
	/**
	 * Create the frame.
	 */
	public ListadoCampanas(IApi api) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				// Obtiene la lista de campañas a mostrar
				obtenerListaDeCampanas();
			}
			@Override
			public void windowClosing(WindowEvent e) {
				cerrar();
			}
		});
		this.api = api;
		setTitle("                                                                                                             Lista de Campañas");
		// Manejamos el cierre de la ventana manualmente.
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 869, 292);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		
		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// Manejar la tecla Delete (Suprimir)
				if(e.getKeyCode() == KeyEvent.VK_DELETE)
					eliminarFilasSeleccionadas();
			}
		});
		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				// Mostrar tooltip de la celda donde se encuentra el puntero.
				Point pos = e.getPoint();
				int fila=table.rowAtPoint(pos),
				    col=table.columnAtPoint(pos);
				if (fila!=-1 && col!=-1)
					table.setToolTipText(table.getValueAt(fila, col).toString());
				else
					table.setToolTipText(null);
			}
		});
		scrollPane.setViewportView(table);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			// Un listener para la selección de filas en la tabla.
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				habilitarBotones();
			}
		});
		
		tblmodel = new DefaultTableModel(titulos,0);
		
		table.setModel(tblmodel);
		table.setDefaultEditor(Object.class, null); // Para no editar las celdas de la tabla.
		

		table.getColumnModel().getColumn(0).setMinWidth(32);
		table.getColumnModel().getColumn(1).setMinWidth(70);
		table.getColumnModel().getColumn(2).setMinWidth(60);
		table.getColumnModel().getColumn(3).setMinWidth(80);
		// Quitar la columna 'Codigo' que es la primera.
		TableColumnModel tcm = table.getColumnModel();
		tcm.removeColumn(tcm.getColumn(0));
		
		panelBotones = new JPanel();
		panelBotones.setBorder(new CompoundBorder(null, new EmptyBorder(10, 10, 10, 10)));
		panelBotones.setPreferredSize(new Dimension(212, 234));
		panelBotones.setBackground(new Color(128, 255, 128));
		contentPane.add(panelBotones, BorderLayout.EAST);
		panelBotones.setLayout(new BoxLayout(panelBotones, BoxLayout.Y_AXIS));
		
		
		
		btnModificar = new JButton("Modificar");
		btnModificar.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnModificar.setPreferredSize(new Dimension(99, 35));
		btnModificar.setMaximumSize(new Dimension(200, 35));
		panelBotones.add(btnModificar);
		
		btnModificar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				///////////     MODIFICAR     UNA CAMPAÑA     ////////////////////
				// Le paso a modificar la api y el código de campaña seleccionada en la tabla
				ModificarCampanaAlt ventanaModificar = new ModificarCampanaAlt(api, (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0));
				boolean res = ventanaModificar.mostrar();
				if (res)
					obtenerListaDeCampanas();
			}
			
		});
		
		verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setPreferredSize(new Dimension(0, 6));
		verticalStrut.setMinimumSize(new Dimension(0, 6));
		verticalStrut.setMaximumSize(new Dimension(32767, 6));
		panelBotones.add(verticalStrut);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnEliminar.setPreferredSize(new Dimension(90, 35));
		btnEliminar.setMaximumSize(new Dimension(200, 35));
		panelBotones.add(btnEliminar);
		
		btnEliminar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				////////////  ELIMINAR CAMPAÑA  //////////////////
				eliminarFilasSeleccionadas();
			}
			
		});
		
		btnEspAsociados = new JButton("Ver Espacios Asociados");
		btnEspAsociados.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnEspAsociados.setPreferredSize(new Dimension(200, 35));
		btnEspAsociados.setMaximumSize(new Dimension(200, 35));
		btnEspAsociados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				///////////////// VER ESPACIOS ASOCIADOS  /////////////////////
				int codCampana = (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0);
			
				ListaEspaciosPublicosAsociados espaciosAsociados = new ListaEspaciosPublicosAsociados(api,codCampana);
				espaciosAsociados.setVisible(true);
			}
		});
		
		verticalStrut_1 = Box.createVerticalStrut(20);
		verticalStrut_1.setPreferredSize(new Dimension(0, 6));
		verticalStrut_1.setMinimumSize(new Dimension(0, 6));
		verticalStrut_1.setMaximumSize(new Dimension(32767, 6));
		panelBotones.add(verticalStrut_1);
		btnEspAsociados.setEnabled(false);
		panelBotones.add(btnEspAsociados);
		
		verticalStrut_3 = Box.createVerticalStrut(20);
		verticalStrut_3.setPreferredSize(new Dimension(0, 6));
		verticalStrut_3.setMinimumSize(new Dimension(0, 6));
		verticalStrut_3.setMaximumSize(new Dimension(32767, 6));
		panelBotones.add(verticalStrut_3);
		
		scrollPane_1 = new JScrollPane();
		panelBotones.add(scrollPane_1);
		
		tableFiltros = new JTable();
		tableFiltros.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		tableFiltros.setModel(new DefaultTableModel(
			new Object[][] {
				{Boolean.FALSE, camposParaFiltrar.get("nombre"), filtrosTexto[1], ""},
				{Boolean.FALSE, camposParaFiltrar.get("descripcion"), filtrosTexto[1], ""},
			},
			new String[] {
				"", "Campo", "Criterio", "Valor"
			}
		) {
			Class[] columnTypes = new Class[] {
				Boolean.class, Item.class, ar.unrn.seminario.gui.common.ItemCriterio.class, Object.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				true, false, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableFiltros.getColumnModel().getColumn(0).setPreferredWidth(15);
		scrollPane_1.setViewportView(tableFiltros);
		tableFiltros.setDefaultEditor(ItemCriterio.class, new DefaultCellEditor(new JComboBox<ItemCriterio>( filtrosTexto )));
		tableFiltros.setDefaultEditor(String.class, new DefaultCellEditor(new JTextField()));
		
		horizontalBox_1 = Box.createHorizontalBox();
		panelBotones.add(horizontalBox_1);
		
		lblOrdenarPor = new JLabel("Ordenar:");
		horizontalBox_1.add(lblOrdenarPor);
		
		btnOrdenAscDesc = new JButton("/\\");
		btnOrdenAscDesc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean invertido = !getDireccionOrdenado();
				btnOrdenAscDesc.putClientProperty(keyDireccionOrdenadoInvertido, invertido);
				btnOrdenAscDesc.setText(invertido ? "\\/" : "/\\");
			}
		});
		btnOrdenAscDesc.putClientProperty(keyDireccionOrdenadoInvertido, false);
		btnOrdenAscDesc.setMaximumSize(new Dimension(30, 25));
		btnOrdenAscDesc.setMargin(new Insets(2, 4, 2, 4));
		btnOrdenAscDesc.setMinimumSize(new Dimension(25, 25));
		horizontalBox_1.add(btnOrdenAscDesc);
		
		cbOrdenarPor = new JComboBox<ItemOrdenado>(camposParaOrdenar);
		cbOrdenarPor.setMinimumSize(new Dimension(90, 24));
		horizontalBox_1.add(cbOrdenarPor);
		
		
		verticalGlue = Box.createVerticalGlue();
		panelBotones.add(verticalGlue);
		
		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		panelBotones.add(progressBar);
		
		verticalStrut_2 = Box.createVerticalStrut(20);
		verticalStrut_2.setPreferredSize(new Dimension(0, 6));
		verticalStrut_2.setMinimumSize(new Dimension(0, 6));
		verticalStrut_2.setMaximumSize(new Dimension(32767, 6));
		panelBotones.add(verticalStrut_2);
		
		horizontalBox = Box.createHorizontalBox();
		panelBotones.add(horizontalBox);
		
		btnFiltrar = new JButton("Filtrar");
		btnFiltrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obtenerListaDeCampanas();
			}
		});
		horizontalBox.add(btnFiltrar);
		
		horizontalGlue = Box.createHorizontalGlue();
		horizontalBox.add(horizontalGlue);
		
		btnVolver = new JButton("Volver");
		horizontalBox.add(btnVolver);
		btnVolver.setAlignmentX(1.0f);
		
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cerrar();
			}
		});
		
		limpiarLista();
		////////////    DESHABILITAR BOTONES    /////////////////
		habilitarBotones();
	}
	
	private void cerrar() {
		if(workerRecuperarLista != null)
			workerRecuperarLista.cancel(true);
		this.dispose();
	}
	
	private void obtenerListaDeCampanas() {
		table.setEnabled(false);
		progressBar.setIndeterminate(true);
		// Realiza la recuperación de datos en segundo plano.
		(workerRecuperarLista = new RecuperarListaWorker()).execute();
	}
	
	private void limpiarLista() {
		tblmodel.setRowCount(0);
	}
	
	private void habilitarBotones() {
		int filasSeleccionadas = table.getSelectedRowCount();
		boolean unaSolaFilaSeleccionada = filasSeleccionadas == 1;
		// Habilitar o deshabilitar botones en función de
		// la cantidad de filas seleccionadas.
		btnEliminar.setEnabled		(filasSeleccionadas > 0);
		btnModificar.setEnabled		(unaSolaFilaSeleccionada);
		btnEspAsociados.setEnabled	(unaSolaFilaSeleccionada);
	}
	
	private void eliminarFilasSeleccionadas() {
		int[] filas = table.getSelectedRows();
		if (filas.length <= 0) return; // No realizar acción si no se seleccionó nada.
		String mensaje = filas.length > 1 ?
				String.format("Estas seguro de eliminar las %d campañas seleccionadas?", filas.length) :
				"Estas seguro de eliminar la campaña seleccionada?";
		// Ventana de confirmación
		if(JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, mensaje, "Confirmar eliminacion", JOptionPane.YES_NO_OPTION)){
			boolean seCambio = false;
			try {
				// Eliminar los registros
				for(int iFila: filas) {
					api.eliminarCampana((Integer) table.getModel().getValueAt(iFila, 0));
					seCambio = true;
				}
			} catch (IntegrityException e) {
				JOptionPane.showMessageDialog(this, "Algún espacio público hace referencia a esta campaña y no se puede eliminar.", "Error al eliminar", JOptionPane.ERROR_MESSAGE);
			} catch (AppException e) {
				Helpers.mostrarMensajeDeError(this, e, "Error al eliminar");
			}
			finally {
				// Actualizar lista
				if(seCambio)
					obtenerListaDeCampanas();
			}
		}
	}
	
	private void onTerminoRecuperarLista(RecuperarListaWorker worker) {
		try {
			List<CampanaDTO> campanas = worker.get();
			limpiarLista();
			// Agrega las campañas en el model
			for (CampanaDTO camp : campanas) {
				tblmodel.addRow(new Object[] { camp.getCodigo(), camp.getNombre(), camp.getFechaEdicion(), camp.getDescripcion() });
			}
		} catch (CancellationException e) {
			if(!this.isVisible()) {
				System.out.print("Se canceló la recuperación de la lista: ");
				System.out.println(e);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// Obtiene la causa de la excepción que se tiró durante la ejecución del Worker.
			Throwable causa = e.getCause();
			Helpers.mostrarMensajeDeError(this, causa, "Error al recuperar datos");
		}
		finally {
			progressBar.setIndeterminate(false);
			table.setEnabled(true);
		}
	}
	
	private class RecuperarListaWorker extends SwingWorker<List<CampanaDTO>,Void> {

		@Override
		protected List<CampanaDTO> doInBackground() throws Exception {
			// Obtiene la lista de campañas a mostrar
			List<CampanaDTO> campanas = api.listarCampanas( getFiltrosSeleccionados(), getOrdenado() );
			return campanas;
		}
		
		@Override
		protected void done() {
			EventQueue.invokeLater( () -> onTerminoRecuperarLista(this) );
		}
		
	}
	
	// Auxiliares
	private Map<String, Filtro> getFiltrosSeleccionados() {
		int filas = tableFiltros.getModel().getRowCount();
		Map<String,Filtro> filtros = new HashMap<>();
		// Iterar en cada fila de la tabla de filtros
		for(int i=0; i<filas; i++) {
			// Elegir solo los filtros con casilla marcada. columna [0]
			if(tableFiltros.getModel().getValueAt(i, 0) == Boolean.TRUE) {
				// Mirar en la columna [2] 'Criterio'
				ItemCriterio criterio = (ItemCriterio)(tableFiltros.getModel().getValueAt(i,2));
				filtros.put(
						// Nombre del campo. columna[1]
						((Item<String>)tableFiltros.getModel().getValueAt(i,1)).getItem(),
						// Crear filtro con el criterio seleccionado y el valor en columna[3]
						criterio.getFiltro( tableFiltros.getModel().getValueAt(i,3).toString() )
						);
			}
		}
		return filtros;
	}
	
	private Ordenado getOrdenado() {
		ItemOrdenado itemOrdenado = (ItemOrdenado)cbOrdenarPor.getSelectedItem();
		return itemOrdenado.getOrdenado( getDireccionOrdenado() );
	}
	
	private boolean getDireccionOrdenado() {
		return (boolean)btnOrdenAscDesc.getClientProperty(keyDireccionOrdenadoInvertido);
	}

}
