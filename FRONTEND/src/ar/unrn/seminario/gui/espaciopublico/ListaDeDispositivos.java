package ar.unrn.seminario.gui.espaciopublico;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;

import java.awt.Color;
import java.awt.BorderLayout;

public class ListaDeDispositivos extends JFrame {

	private JPanel contentPane;
	private DefaultTableModel tblmodel;
	private JScrollPane scrollPane;
	private JTable table;
	private String[] titulos = {"Codigo Dispositivo", "Nombre Dispositivo"};

	/**
	 * Create the frame.
	 */
	public ListaDeDispositivos(IApi api, int codEspacio) {
		setTitle("                                                               Lista de Dispositivos Asosiciados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 342);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		tblmodel = new DefaultTableModel(titulos,0);
		table.setModel(tblmodel);
		
		// Obtiene la lista de dispositivos a mostrar
		List<DispositivoDTO> dispositivos = api.listarDispositivosDeUnEspacio(codEspacio);
		for(DispositivoDTO d: dispositivos) {
			// Agrega los dispositivos en el model
			tblmodel.addRow(new Object[] {d.getCodigo(), d.getNombre()});
		}
		
	}
}
