package ar.unrn.seminario.gui.espaciopublico;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;

public class ModificarEspacioPublicoAlt extends AltaEspacioPublico {
	
	private boolean resultado = false;
	private EspacioPublicoDTO espacio;
	private JButton btnModificar;

	/**
	 * MODIFICAR
	 */
	public ModificarEspacioPublicoAlt(IApi api, Integer CodigoEspacio) {
		super(api);
		this.tituloMsjError = "Error al registrar el espacio público";
		setTitle("Modificar Espacio Publico");

		espacio = api.obtenerEspacioPublicoPorCodigo(CodigoEspacio);
		
		btnModificar = super.btnCrear;
		btnModificar.setText("Modificar");
		
		comboBoxCampana.removeItemAt(0);
		comboBoxTipo.removeItemAt(0);
		
		setearDatos();
	}
	
	private void setearDatos() {

		//ASIGNO Nombre AL RECUADRO Nombre
		txtNombre.setText(espacio.getNombre());
		//ASIGNO Direccion AL RECUADRO Direccion
		txtDireccion.setText(espacio.getDireccion());
		//ASIGNO Direccion AL RECUADRO Direccion
		txtDireccion.setText(espacio.getDireccion());
		//ASIGNO ZonaDelimitadaLatitud AL RECUADRO txtZonaDelimitadaLatitud
		txtZonaDelimitadaLatitud.setText(Double.toString(espacio.getLatitud()));
		txtZonaDelimitadaLongitud.setText(Double.toString(espacio.getLongitud()));
		txtZonaDelimitadaRadio.setText(Double.toString(espacio.getRadio()));
		//ASIGNO QR 
		txtCodigoQR.setText(espacio.getQr());
		//ASIGNO IMAGEN
		txtIMG.setText(espacio.getUrlImagen());
		txtDescripcion.setText(espacio.getDescripcion());
		
		//Asignar campana y tipo en los respectivos JComboBox(es)
		comboBoxCampana.setSelectedItem(new ItemCampanaExistente(api.obtenerCampanaPorCodigo(espacio.getCodigoCampana())));
		comboBoxTipo.setSelectedItem(new ItemTipoExistente(api.obtenerTipoPorCodigo(espacio.getCodigoTipo())));

	}
	
	public boolean mostrar() {
		this.setModal(true);
		this.setVisible(true);
		return resultado;
	}
	
	protected boolean enviar() {
		return resultado = super.enviar();
	}
	
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Espacio Modificado con Éxito", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	@Override
	protected boolean registrar(String nombre, String direccion, double latitud, double longitud, double radio, Integer codigoTipo, String descripcion, String urlImagen, String qr, Integer codigoCampana) {
		api.modificarEspacioPublico(espacio.getCodigo(), nombre, direccion, longitud, latitud, radio, codigoTipo, descripcion, urlImagen, qr, codigoCampana);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}
}
