package ar.unrn.seminario.gui.espaciopublico;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

import ar.edu.unrn.seminario.api.Filtro;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.Ordenado;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.unrn.seminario.gui.common.Helpers;
import ar.unrn.seminario.gui.common.Item;
import ar.unrn.seminario.gui.common.ItemCriterio;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingWorker;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JProgressBar;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.CompoundBorder;
import javax.swing.Box;
import java.awt.Component;
import javax.swing.JLabel;
import java.awt.Insets;
import javax.swing.JComboBox;
import ar.unrn.seminario.gui.common.ItemOrdenado;

public class ListadoEspaciosPublicos extends JFrame {

	private IApi api;
	private JPanel contentPane, panelBotones;
	private DefaultTableModel tblmodel;
	private JScrollPane scrollPane;
	private JButton btnModificar, btnEliminar, btnDispAsignados, btnVerMapa, btnVolver;
	private JTable table;
	private JProgressBar progressBar;
	private RecuperarListaWorker workerRecuperarLista;
	
	private String[] titulos = {"Codigo","Nombre","Direccion","Tipo", "Descripcion", "Campaña"};
	private Box horizontalBox;
	private Component verticalGlue;
	private Component horizontalGlue;
	private Component verticalStrut;
	private Component verticalStrut_1;
	private Component verticalStrut_2;
	private Component verticalStrut_3;
	private JScrollPane scrollPane_1;
	private Box horizontalBox_1;
	private JLabel lblOrdenarPor;
	private JButton btnOrdenAscDesc;
	private JComboBox<ItemOrdenado> cbOrdenarPor;
	private JTable tableFiltros;

	private ItemCriterio[] filtrosTexto = {
			new ItemCriterio("es", (o) -> Filtro.es(o)),
			new ItemCriterio("contiene", (o) -> Filtro.contiene((String)o)),
			new ItemCriterio("empieza con", (o) -> Filtro.comienzaCon((String)o)),
			new ItemCriterio("termina con", (o) -> Filtro.terminaCon((String)o))
	};
	private ItemCriterio[] filtrosCodigo = {
			new ItemCriterio("es", (o) -> Filtro.es( ((ItemCodigo)o).getCodigo()) ),
			new ItemCriterio("no es", (o) -> Filtro.no(Filtro.es( ((ItemCodigo)o).getCodigo()) )),
	};
	private Map<String, Item<String>> camposParaFiltrar = Map.of(
			"nombre", new Item<String>("nombre", "Nombre"),
			"descripcion", new Item<String>("descripcion", "Descripción"),
			"direccion", new Item<String>("direccion", "Dirección"),
			"codigoTipo", new Item<String>("codigoTipo", "Tipo")
			);
	
	private ItemOrdenado[] camposParaOrdenar = {
			new ItemOrdenado(null, "(sin ordenar)"),
			new ItemOrdenado("nombre", "Nombre"),
			new ItemOrdenado("codigo", "Código"),
			//new ItemOrdenado("fechaEdicion", "Fecha edición"),
	};
	private final String keyDireccionOrdenadoInvertido = "Ordenado.invertido";
	private JButton btnFiltrar;
	private JComboBox<ItemCodigo> filtrosListaTipo = new JComboBox<>();
	
	
	/**
	 * Create the frame.
	 */
	public ListadoEspaciosPublicos(IApi api) {

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				// Obtiene la lista de campañas a mostrar
				obtenerListaDeEspacios();
			}
			@Override
			public void windowClosing(WindowEvent e) {
				cerrar();
			}
		});
		this.api = api;
		setTitle("                                                                                                        Lista de Espacios Publicos");
		// Manejamos el cierre de la ventana manualmente.
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 927, 342);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// Manejar la tecla Delete (Suprimir)
				if(e.getKeyCode() == KeyEvent.VK_DELETE)
					eliminarFilasSeleccionadas();
			}
		});
		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				// Mostrar tooltip de la celda donde se encuentra el puntero.
				Point pos = e.getPoint();
				int fila=table.rowAtPoint(pos),
				    col=table.columnAtPoint(pos);
				if (fila!=-1 && col!=-1)
					table.setToolTipText(table.getValueAt(fila, col).toString());
				else
					table.setToolTipText(null);
			}
		});
		scrollPane.setViewportView(table);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			// Un listener para la selección de filas en la tabla.
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				habilitarBotones();
			}
		});
		
		tblmodel = new DefaultTableModel(titulos,0);
		table.setModel(tblmodel);
		table.setDefaultEditor(Object.class, null); // Para no editar las celdas de la tabla.
		table.getColumnModel().getColumn(0).setMinWidth(32);
		table.getColumnModel().getColumn(1).setMinWidth(80);
		table.getColumnModel().getColumn(2).setMinWidth(60);
		table.getColumnModel().getColumn(3).setMinWidth(40);
		table.getColumnModel().getColumn(4).setMinWidth(60);
		table.getColumnModel().getColumn(5).setMinWidth(80);
		// Quitar la columna 'Codigo' que es la primera.
		TableColumnModel tcm = table.getColumnModel();
		tcm.removeColumn(tcm.getColumn(0));
		
		panelBotones = new JPanel();
		panelBotones.setBorder(new CompoundBorder(null, new EmptyBorder(10, 10, 10, 10)));
		panelBotones.setMinimumSize(new Dimension(261, 100));
		panelBotones.setPreferredSize(new Dimension(256, 32));
		panelBotones.setBackground(new Color(128, 255, 128));
		contentPane.add(panelBotones, BorderLayout.EAST);
		panelBotones.setLayout(new BoxLayout(panelBotones, BoxLayout.PAGE_AXIS));
		
		btnModificar = new JButton("Modificar");
		btnModificar.setAlignmentX(0.5f);
		btnModificar.setMaximumSize(new Dimension(999, 35));
		panelBotones.add(btnModificar);
		
		btnModificar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				// Le paso a modificar la api y el código de espacioPublico seleccionada en la tabla
				ModificarEspacioPublicoAlt modificarespacio = new ModificarEspacioPublicoAlt(api, (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0));
				boolean res = modificarespacio.mostrar();
				if (res)
					obtenerListaDeEspacios();
			}
			
		});
		
		verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setPreferredSize(new Dimension(0, 10));
		verticalStrut.setMinimumSize(new Dimension(0, 5));
		verticalStrut.setMaximumSize(new Dimension(32767, 10));
		panelBotones.add(verticalStrut);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setAlignmentX(0.5f);
		btnEliminar.setMaximumSize(new Dimension(999, 35));
		panelBotones.add(btnEliminar);
		
		btnEliminar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				eliminarFilasSeleccionadas();
			}
			
		});
		
		verticalStrut_1 = Box.createVerticalStrut(20);
		verticalStrut_1.setPreferredSize(new Dimension(0, 10));
		verticalStrut_1.setMinimumSize(new Dimension(0, 5));
		verticalStrut_1.setMaximumSize(new Dimension(32767, 10));
		panelBotones.add(verticalStrut_1);
		
		btnDispAsignados = new JButton("Ver Disp. Asignados");
		btnDispAsignados.setEnabled(false);
		btnDispAsignados.setAlignmentX(0.5f);
		btnDispAsignados.setMaximumSize(new Dimension(999, 35));
		panelBotones.add(btnDispAsignados);
		
		btnDispAsignados.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				///// DISPOSITIVOS ASIGNADOS    /////////
				Integer codEspacio = (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0);
				
				ListaDeDispositivos dispositivosAsociados = new ListaDeDispositivos(api,codEspacio);
				dispositivosAsociados.setVisible(true);
				
			}
			
		});
		
		btnVerMapa = new JButton("Ver en Mapa");
		btnVerMapa.setAlignmentX(0.5f);
		btnVerMapa.setMaximumSize(new Dimension(999, 35));
		btnVerMapa.setVisible(true);
		
		verticalStrut_2 = Box.createVerticalStrut(20);
		verticalStrut_2.setPreferredSize(new Dimension(0, 10));
		verticalStrut_2.setMinimumSize(new Dimension(0, 5));
		verticalStrut_2.setMaximumSize(new Dimension(32767, 10));
		panelBotones.add(verticalStrut_2);
		panelBotones.add(btnVerMapa);
		
		btnVerMapa.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				abrirMapa();
				
			}
			
		});
		
		verticalStrut_3 = Box.createVerticalStrut(20);
		verticalStrut_3.setPreferredSize(new Dimension(0, 10));
		verticalStrut_3.setMinimumSize(new Dimension(0, 5));
		verticalStrut_3.setMaximumSize(new Dimension(32767, 10));
		panelBotones.add(verticalStrut_3);
		
		scrollPane_1 = new JScrollPane();
		panelBotones.add(scrollPane_1);
		
		tableFiltros = new JTable() {
			private TableCellEditor editorObjeto = new DefaultCellEditor(new JComboBox<ItemCriterio>(filtrosCodigo));
			private TableCellEditor editorTipo = new DefaultCellEditor(filtrosListaTipo);
			public TableCellEditor getCellEditor(int row, int column) {
				// Ya sé que no es la mejor solución pero me estaba quedando sin tiempo.
				if (row == 3) { // Fila tipo
					switch(column) {
					case 2:
						return editorObjeto;
					case 3:
						return editorTipo;
					}
				}
				return getDefaultEditor(getColumnClass(column));
			}
		};
		tableFiltros.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		tableFiltros.setModel(new DefaultTableModel(
			new Object[][] {
				{Boolean.FALSE, camposParaFiltrar.get("nombre"), filtrosTexto[1], ""},
				{Boolean.FALSE, camposParaFiltrar.get("descripcion"), filtrosTexto[1], ""},
				{Boolean.FALSE, camposParaFiltrar.get("direccion"), filtrosTexto[1], ""},
				{Boolean.FALSE, camposParaFiltrar.get("codigoTipo"), filtrosCodigo[0], new ItemTipoVacio("(seleccionar)")},
			},
			new String[] {
				"", "Campo", "Criterio", "Valor"
			}
		) {
			Class[] columnTypes = new Class[] {
				Boolean.class, Item.class, ar.unrn.seminario.gui.common.ItemCriterio.class, Object.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				true, false, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableFiltros.getColumnModel().getColumn(0).setPreferredWidth(15);
		scrollPane_1.setViewportView(tableFiltros);
		tableFiltros.setDefaultEditor(ItemCriterio.class, new DefaultCellEditor(new JComboBox<ItemCriterio>( filtrosTexto )));
		tableFiltros.setDefaultEditor(String.class, new DefaultCellEditor(new JTextField()));
		
		horizontalBox_1 = Box.createHorizontalBox();
		panelBotones.add(horizontalBox_1);
		
		lblOrdenarPor = new JLabel("Ordenar:");
		horizontalBox_1.add(lblOrdenarPor);
		
		btnOrdenAscDesc = new JButton("/\\");
		btnOrdenAscDesc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean invertido = !getDireccionOrdenado();
				btnOrdenAscDesc.putClientProperty(keyDireccionOrdenadoInvertido, invertido);
				btnOrdenAscDesc.setText(invertido ? "\\/" : "/\\");
			}
		});
		btnOrdenAscDesc.putClientProperty(keyDireccionOrdenadoInvertido, false);
		btnOrdenAscDesc.setMinimumSize(new Dimension(25, 25));
		btnOrdenAscDesc.setMaximumSize(new Dimension(30, 25));
		btnOrdenAscDesc.setMargin(new Insets(2, 4, 2, 4));
		horizontalBox_1.add(btnOrdenAscDesc);
		
		cbOrdenarPor = new JComboBox<ItemOrdenado>(camposParaOrdenar);
		cbOrdenarPor.setMinimumSize(new Dimension(90, 24));
		horizontalBox_1.add(cbOrdenarPor);
		
		verticalGlue = Box.createVerticalGlue();
		panelBotones.add(verticalGlue);
		
		progressBar = new JProgressBar();
		panelBotones.add(progressBar);
		
		horizontalBox = Box.createHorizontalBox();
		panelBotones.add(horizontalBox);
		
		btnFiltrar = new JButton("Filtrar");
		btnFiltrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obtenerListaDeEspacios();
			}
		});
		horizontalBox.add(btnFiltrar);
		
		horizontalGlue = Box.createHorizontalGlue();
		horizontalBox.add(horizontalGlue);
		
		btnVolver = new JButton("Volver");
		horizontalBox.add(btnVolver);
		btnVolver.setAlignmentX(0.5f);
		
		btnVolver.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});

		limpiarLista();
		////////////DESHABILITAR BOTONES    /////////////////
		habilitarBotones();
	}
	
	private void cerrar() {
		if(workerRecuperarLista != null)
			workerRecuperarLista.cancel(true);
		this.dispose();
	}
	
	private void obtenerListaDeEspacios() {
		table.setEnabled(false);
		progressBar.setIndeterminate(true);
		// Realiza la recuperación de datos en segundo plano.
		(workerRecuperarLista = new RecuperarListaWorker()).execute();
	}
	
	private void limpiarLista() {
		tblmodel.setRowCount(0);
	}
	
	private void habilitarBotones() {
		int filasSeleccionadas = table.getSelectedRowCount();
		boolean unaSolaFilaSeleccionada = filasSeleccionadas == 1;
		// Habilitar o deshabilitar botones en función de
		// la cantidad de filas seleccionadas.
		btnEliminar.setEnabled		(filasSeleccionadas > 0);
		btnModificar.setEnabled		(unaSolaFilaSeleccionada);
		btnDispAsignados.setEnabled		(unaSolaFilaSeleccionada);
		btnVerMapa.setEnabled		(unaSolaFilaSeleccionada);
	}
	
	private void eliminarFilasSeleccionadas() {
		int[] filas = table.getSelectedRows();
		if (filas.length <= 0) return; // No realizar acción si no se seleccionó nada.
		String mensaje = filas.length > 1 ?
				String.format("Estas seguro de eliminar los %d espacios publicos seleccionados?", filas.length) :
				"Estas seguro de eliminar el espacio publico seleccionado?";
		// Ventana de confirmación
		if(JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, mensaje, "Confirmar eliminacion", JOptionPane.YES_NO_OPTION)){
			boolean seCambio = false;
			try {
				// Eliminar los registros
				for(int iFila: filas) {
					api.eliminarEspacioPublico((Integer) table.getModel().getValueAt(iFila, 0));
					seCambio = true;
				}
			} catch(IntegrityException e) {
				JOptionPane.showMessageDialog(this, "Algún dispositivo hace referencia a este espacio y no se puede eliminar.", "Error al eliminar", JOptionPane.ERROR_MESSAGE);
			} catch (AppException e) {
				Helpers.mostrarMensajeDeError(this, e, "Error al eliminar");
			} finally {
				// Actualizar lista
				if(seCambio)
					obtenerListaDeEspacios();
			}
		}
	}
	
	private void onTerminoRecuperarLista(RecuperarListaWorker worker) {
		try {
			List<EspacioPublicoDTO> espacios = worker.get();
			limpiarLista();
			// Agrega los espacios en el model
			for (EspacioPublicoDTO ep : espacios) {
				tblmodel.addRow(new Object[] {ep.getCodigo(), ep.getNombre(), ep.getDireccion(), ep.getTipo(), ep.getDescripcion(), ep.getNombreCampana() });
			}
		} catch (CancellationException e) {
			if(!this.isVisible()) {
				System.out.print("Se canceló la recuperación de la lista: ");
				System.out.println(e);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// Obtiene la causa de la excepción que se tiró durante la ejecución del Worker.
			Throwable causa = e.getCause();
			// Arma el mensaje incluyendo más detalles.
			StringBuffer sb = new StringBuffer(causa.getMessage());
			causa = causa.getCause();
			if(causa != null) sb.append("\n");
			while(causa != null) {
				sb.append("\n");
				sb.append(causa.toString());
				causa = causa.getCause();
			}
			// Muestra el mensaje de error en un cuadro si la ventana es visible.
			if(this.isVisible())
				JOptionPane.showMessageDialog(this, sb, "Error al recuperar datos", JOptionPane.ERROR_MESSAGE);
			else
				System.err.println(sb);
		}
		finally {
			progressBar.setIndeterminate(false);
			table.setEnabled(true);
		}
	}
	
	private class RecuperarListaWorker extends SwingWorker<List<EspacioPublicoDTO>,Void> {

		@Override
		protected List<EspacioPublicoDTO> doInBackground() throws Exception {
			// Obtiene la lista de espacios a mostrar
			List<EspacioPublicoDTO> espacios = api.listarEspaciosPublicos( getFiltrosSeleccionados(), getOrdenado() );
			// De paso, la lista de tipos
			List<TipoDTO> tipos = api.listarTipos();
			ItemCodigo[] arrayTipos = tipos.stream().map( (tipo) -> new ItemCodigo() {
				@Override
				Integer getCodigo() {
					return tipo.getCodigo();
				}

				@Override
				String getNombre() {
					return tipo.getNombre();
				}
				
				public String toString() {
					return getNombre();
				}
			}).toArray(ItemCodigo[]::new);
			filtrosListaTipo.setModel(new DefaultComboBoxModel<ItemCodigo>(arrayTipos));
			return espacios;
		}
		
		@Override
		protected void done() {
			EventQueue.invokeLater( () -> onTerminoRecuperarLista(this) );
		}
		
	}
	
	// Auxiliares
	private Map<String, Filtro> getFiltrosSeleccionados() {
		int filas = tableFiltros.getModel().getRowCount();
		Map<String,Filtro> filtros = new HashMap<>();
		// Iterar en cada fila de la tabla de filtros
		for(int i=0; i<filas; i++) {
			// Elegir solo los filtros con casilla marcada. columna [0]
			if(tableFiltros.getModel().getValueAt(i, 0) == Boolean.TRUE) {
				Object valorFiltro = tableFiltros.getModel().getValueAt(i,3); // valor en columna[3]
				if(valorFiltro != null) {
					// Mirar en la columna [2] 'Criterio'
					ItemCriterio criterio = (ItemCriterio)(tableFiltros.getModel().getValueAt(i,2));
					filtros.put(
							// Nombre del campo. columna[1]
							((Item<String>)tableFiltros.getModel().getValueAt(i,1)).getItem(),
							// Crear filtro con el criterio seleccionado y el valor en columna[3]
							criterio.getFiltro( valorFiltro )
							);
				}
			}
		}
		return filtros;
	}
	
	private Ordenado getOrdenado() {
		ItemOrdenado itemOrdenado = (ItemOrdenado)cbOrdenarPor.getSelectedItem();
		return itemOrdenado.getOrdenado( getDireccionOrdenado() );
	}
	
	private boolean getDireccionOrdenado() {
		return (boolean)btnOrdenAscDesc.getClientProperty(keyDireccionOrdenadoInvertido);
	}
	
	private void abrirMapa () {
		try {
			Integer codEspacio = (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0);
			String url = "https://www.google.com/maps/place/" + api.obtenerEspacioPublicoPorCodigo(codEspacio).getLatitud() 
					+ "," + api.obtenerEspacioPublicoPorCodigo(codEspacio).getLongitud();
			Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, "Ocurrió un error inesperado al querer abrir el mapa.", "Error vista mapa", JOptionPane.ERROR_MESSAGE);
		}
	}

}
