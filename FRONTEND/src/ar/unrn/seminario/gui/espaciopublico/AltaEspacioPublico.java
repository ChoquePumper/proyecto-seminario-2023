package ar.unrn.seminario.gui.espaciopublico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Objects;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.unrn.seminario.gui.common.Helpers;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;
import java.awt.Color;

public class AltaEspacioPublico extends JDialog {

	protected IApi api;
	protected String tituloMsjError;
	
	private JPanel contentPane, panelBotones;
	protected JTextField txtNombre,txtDireccion, txtZonaDelimitadaLatitud, txtCodigoQR, txtIMG;
	private JLabel lblNombre,lblCampaña, lblDescripcion,lblZonaDelimitada, lblTipo, lblDireccion,lblCodigoQR, lblImagen; 
	protected JTextArea txtDescripcion;
	protected JButton btnCrear, btnCancelar;
	protected JTextField txtZonaDelimitadaLongitud;
	protected JTextField txtZonaDelimitadaRadio;
	protected JComboBox<ItemCodigo> comboBoxCampana;
	protected JComboBox<ItemCodigo> comboBoxTipo;

	

	/**
	 * Create the frame.
	 */
	public AltaEspacioPublico(IApi api) {
		this.api = api;
		this.tituloMsjError = "Error al registrar el espacio público";
		setTitle("Crear Espacio Publico");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 431, 522);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 24, 81, 14);
		contentPane.add(lblNombre);
		
		lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(10, 55, 64, 14);
		contentPane.add(lblDireccion);
		
		lblZonaDelimitada = new JLabel("Zona Delimitada: ");
		lblZonaDelimitada.setBounds(10, 89, 108, 14);
		contentPane.add(lblZonaDelimitada);
		
		lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(10, 152, 46, 14);
		contentPane.add(lblTipo);
		
		lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(10, 261, 81, 14);
		contentPane.add(lblDescripcion);
		
		lblCampaña = new JLabel("Campaña:");
		lblCampaña.setBounds(10, 207, 81, 14);
		contentPane.add(lblCampaña);
		
		lblCodigoQR = new JLabel("Codigo QR:");
		lblCodigoQR.setBounds(10, 363, 81, 14);
		contentPane.add(lblCodigoQR);
		
		lblImagen = new JLabel("Imagen:");
		lblImagen.setBounds(10, 311, 81, 14);
		contentPane.add(lblImagen);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(116, 21, 165, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(116, 52, 165, 20);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		txtZonaDelimitadaLatitud = new JTextField();
		txtZonaDelimitadaLatitud.setBounds(116, 108, 86, 20);
		contentPane.add(txtZonaDelimitadaLatitud);
		txtZonaDelimitadaLatitud.setColumns(10);
		
		txtCodigoQR = new JTextField();
		txtCodigoQR.setBounds(116, 360, 165, 20);
		contentPane.add(txtCodigoQR);
		txtCodigoQR.setColumns(10);
		
		txtIMG = new JTextField();
		txtIMG.setBounds(116, 308, 165, 20);
		contentPane.add(txtIMG);
		txtIMG.setColumns(10);
		
		txtDescripcion = new JTextArea();
		txtDescripcion.setBounds(116, 256, 165, 32);
		contentPane.add(txtDescripcion);
		
		panelBotones = new JPanel();
		panelBotones.setBackground(new Color(128, 255, 128));
		panelBotones.setForeground(Color.BLACK);
		panelBotones.setBounds(10, 413, 395, 59);
		contentPane.add(panelBotones);
		panelBotones.setLayout(null);
	
		btnCrear = new JButton("Crear");
		btnCrear.setBounds(197, 11, 89, 37);
		panelBotones.add(btnCrear);
		
		btnCrear.addActionListener(getAccion());
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(296, 11, 89, 37);
		panelBotones.add(btnCancelar);
		
		txtZonaDelimitadaLongitud = new JTextField();
		txtZonaDelimitadaLongitud.setColumns(10);
		txtZonaDelimitadaLongitud.setBounds(212, 108, 86, 20);
		contentPane.add(txtZonaDelimitadaLongitud);
		
		txtZonaDelimitadaRadio = new JTextField();
		txtZonaDelimitadaRadio.setColumns(10);
		txtZonaDelimitadaRadio.setBounds(308, 108, 64, 20);
		contentPane.add(txtZonaDelimitadaRadio);
		
		JLabel lblLatitud = new JLabel("Latitud");
		lblLatitud.setBounds(140, 89, 39, 14);
		contentPane.add(lblLatitud);
		
		JLabel lblLongitud = new JLabel("Longitud");
		lblLongitud.setBounds(235, 89, 56, 14);
		contentPane.add(lblLongitud);
		
		JLabel lblRadio = new JLabel("Radio");
		lblRadio.setBounds(323, 89, 39, 14);
		contentPane.add(lblRadio);
		
		comboBoxCampana = new JComboBox<>();
		comboBoxCampana.setBounds(116, 203, 165, 22);
		contentPane.add(comboBoxCampana);
		
		comboBoxTipo = new JComboBox<>();
		comboBoxTipo.setBounds(116, 149, 165, 22);
		contentPane.add(comboBoxTipo);
		
		comboBoxCampana.addItem( new ItemCampanaVacia("(seleccionar)") );
		for (CampanaDTO camp : api.listarCampanas(null)) {
			comboBoxCampana.addItem( new ItemCampanaExistente(camp) );
		}
		
		comboBoxTipo.addItem( new ItemTipoVacio("(seleccionar)") );
		for (TipoDTO tipo : api.listarTipos()) {
			comboBoxTipo.addItem( new ItemTipoExistente(tipo) );
		}
		
		btnCancelar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});
		
		

	}
	
	protected ActionListener accion = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e) {
			if(enviar()) dispose();
		}
	};
	
	protected ActionListener getAccion() {
		return accion;
	}
	
	protected boolean enviar() {
		boolean exito = false;
		try {
			// Preparar datos
			double latitud = Double.parseDouble(txtZonaDelimitadaLatitud.getText());
			double longitud = Double.parseDouble(txtZonaDelimitadaLongitud.getText());
			double radio = Double.parseDouble(txtZonaDelimitadaRadio.getText());
			
			exito = registrar(txtNombre.getText(), txtDireccion.getText(), latitud, longitud, radio, ((ItemCodigo)comboBoxTipo.getSelectedItem()).getCodigo(), txtDescripcion.getText(), txtIMG.getText(), txtCodigoQR.getText(), ((ItemCodigo)comboBoxCampana.getSelectedItem()).getCodigo());
			
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Los datos de Latitud/Longitud/Radio son incorrectos.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DataEmptyException e) {
			String msj = String.format("El campo '%s' está vacío", e.getNombreCampo());
			msj = msjErrorPorCampo.getOrDefault(e.getNombreCampo(), msj);
			JOptionPane.showMessageDialog(this, msj, tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DuplicateException e) {
			JOptionPane.showMessageDialog(this, "Ya existe un espacio público con el mismo nombre.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DataAccessException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "No se pudo acceder a los datos.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (AppException e) {
			//JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			Helpers.mostrarMensajeDeError(this, e, tituloMsjError);
		}
		if(exito) onEnvioExitoso();
		return exito;
	}
	
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Espacio Registrado con Éxito", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	protected boolean registrar(String nombre, String direccion, double latitud, double longitud, double radio, Integer codigoTipo, String descripcion, String urlImagen, String qr, Integer codigoCampana) {
		api.registrarEspacioPublico(null, nombre, direccion, longitud, latitud, radio, codigoTipo, descripcion, urlImagen, qr, codigoCampana);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}
	
	private Map<String,String> msjErrorPorCampo = Map.of(
			"campaña", "No se especificó la campaña.",
			"tipo", "No se especificó el tipo de espacio público.");
}

/** Elemento para JComboBox. */
abstract class ItemCodigo {
	abstract Integer getCodigo();
	abstract String getNombre();
	public int hashCode() { return Objects.hashCode(getCodigo()); }
	/** @apiNote El método setSelectedItem de JComboBox depende del equals(). */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (this.getClass() != obj.getClass())
			return false;
		ItemCodigo o = (ItemCodigo)obj;
		return this.getCodigo() == o.getCodigo();
	}
}

class ItemCampanaExistente extends ItemCodigo {
	private final CampanaDTO campana;
	
	ItemCampanaExistente(CampanaDTO campana) {
		this.campana = campana;
	}

	public Integer getCodigo() { return campana.getCodigo(); }
	public String getNombre() { return campana.getNombre(); }
	
	@Override
	public String toString() {
		return String.format("Cód %s: %s", getCodigo(), getNombre());
	}
}

final class ItemCampanaVacia extends ItemCodigo {
	private String textoAMostrar;
	public ItemCampanaVacia() { this(null); }
	public ItemCampanaVacia(String texto) {
		this.textoAMostrar = Objects.requireNonNullElse(texto, "(vacío)");
	}
	public Integer getCodigo() { return null; }
	public String getNombre() { return null; }
	public String toString() { return textoAMostrar; }
}

class ItemTipoExistente extends ItemCodigo {
	private final TipoDTO tipo;
	
	ItemTipoExistente(TipoDTO tipo) {
		this.tipo = tipo;
	}

	public Integer getCodigo() { return tipo.getCodigo(); }
	public String getNombre() { return tipo.getNombre(); }
	
	@Override
	public String toString() {
		return String.format("Cód %s: %s", getCodigo(), getNombre());
	}
}

final class ItemTipoVacio extends ItemCodigo {
	private String textoAMostrar;
	public ItemTipoVacio() { this(null); }
	public ItemTipoVacio(String texto) {
		this.textoAMostrar = Objects.requireNonNullElse(texto, "(vacío)");
	}
	public Integer getCodigo() { return null; }
	public String getNombre() { return null; }
	public String toString() { return textoAMostrar; }
}
