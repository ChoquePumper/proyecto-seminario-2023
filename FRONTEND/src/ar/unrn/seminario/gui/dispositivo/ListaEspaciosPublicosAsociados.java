package ar.unrn.seminario.gui.dispositivo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;

public class ListaEspaciosPublicosAsociados extends JFrame {


	private JPanel contentPane;
	private DefaultTableModel tblmodel;
	private JScrollPane scrollPane;
	private JTable table;
	private String[] titulos = {"Nombre","Estado","Descripción","Modelo","Marca"};

	/**
	 * Create the frame.
	 */
	public ListaEspaciosPublicosAsociados(IApi api, int codEspacio) {
		setTitle("                                                               Lista de Espacios Publicos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 386);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		tblmodel = new DefaultTableModel(titulos,0);
		table.setModel(tblmodel);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setPreferredSize(new Dimension(80, 25));
		btnVolver.setMaximumSize(new Dimension(80, 25));
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		contentPane.add(btnVolver, BorderLayout.SOUTH);
		
		List<EspacioPublicoDTO> dispositivosAsociados = api.listarEspacioAsociadoDispositivos(codEspacio);
		
		// Agrega los espacios en el model
		for (EspacioPublicoDTO ep : dispositivosAsociados) {
			tblmodel.addRow(new Object[] { ep.getNombre(), ep.getDireccion(), ep.getTipo(), ep.getDescripcion() });
		}

	}

}
