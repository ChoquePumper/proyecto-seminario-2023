package ar.unrn.seminario.gui.dispositivo;

import java.awt.EventQueue;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.unrn.seminario.gui.dispositivo.AltaDispositivo.ItemCodigo;

public class ModificarDispositivoAlt extends AltaDispositivo {

	private boolean resultado = false;
	private DispositivoDTO disp;
	public ModificarDispositivoAlt(IApi api, Integer codigoDispositivo) {
		super(api);

		this.tituloMsjError = "Error al modificar el dispositivo";
		this.disp = api.obtenerDispositivoPorCodigo(codigoDispositivo);
		
		// Retocar la GUI.
		setTitle("                     Modificar Dispositivo");
		btnCrear.setText("Modificar");
		
		// Remover elementos (seleccionar)
		comboBoxEspacio.removeItemAt(0);

		txtNombre.setText(disp.getNombre());
		ActivoCheckBox.setSelected(disp.getEstado());
		txtDescripcion.setText(disp.getDescripcion());
		txtModelo.setText(disp.getModelo());
		txtMarca.setText(disp.getMarca());

		//Asignar espacio público en el JComboBox
		comboBoxEspacio.setSelectedItem(new ItemEspacioExistente(api.obtenerEspacioPublicoPorCodigo(disp.getCodigoEspacio())));
	
	}
	
	public boolean mostrar() {
		this.setModal(true);
		this.setVisible(true);
		return resultado;
	}
	
	@Override
	protected boolean enviar() {
		resultado = super.enviar();
		return resultado;
	}
	
	@Override
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Tipo modificado con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	@Override
	protected boolean registrar(String nombre, Boolean estado, String descripcion, String modelo, String marca, Integer codEspacio) {
		api.modificarDispositivo(disp.getCodigo(), nombre, estado, descripcion, modelo, marca, codEspacio);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}


}
