package ar.unrn.seminario.gui.dispositivo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.exception.AppException;
import ar.unrn.seminario.gui.campana.ModificarCampanaAlt;
import ar.unrn.seminario.gui.common.Helpers;

public class ListadoDispositivos extends JFrame {

	private IApi api;
	private JPanel contentPane, panelBotones;
	private DefaultTableModel tblmodel;
	private JScrollPane scrollPane;
	private JButton btnModificar, btnEliminar, btnVolver;
	private JTable table;
	private JProgressBar progressBar;
	private RecuperarListaWorker workerRecuperarLista;
	
	private String[] titulos = {"Codigo","Nombre","Estado","Descripción","Modelo", "Marca", "Espacio Asignado"};

	/**
	 * Create the frame.
	 */
	public ListadoDispositivos(IApi api) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				// Obtiene la lista de dispositivos a mostrar
				obtenerListaDispositivos();
			}
			@Override
			public void windowClosing(WindowEvent e) {
				cerrar();
			}
		});
		this.api = api;
		setTitle("Lista de Dispositivos");
		// Manejamos el cierre de la ventana manualmente.
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 885, 292);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		
		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// Manejar la tecla Delete (Suprimir)
				if(e.getKeyCode() == KeyEvent.VK_DELETE)
					eliminarFilasSeleccionadas();
			}
		});
		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				// Mostrar tooltip de la celda donde se encuentra el puntero.
				Point pos = e.getPoint();
				int fila=table.rowAtPoint(pos),
				    col=table.columnAtPoint(pos);
				if (fila!=-1 && col!=-1)
					table.setToolTipText(table.getValueAt(fila, col).toString());
				else
					table.setToolTipText(null);
			}
		});
		scrollPane.setViewportView(table);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			// Un listener para la selección de filas en la tabla.
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				habilitarBotones();
			}
		});
		
		tblmodel = new DefaultTableModel(titulos,0);
		
		table.setModel(tblmodel);
		table.setDefaultEditor(Object.class, null); // Para no editar las celdas de la tabla.
		

		table.getColumnModel().getColumn(0).setMinWidth(32);
		table.getColumnModel().getColumn(1).setMinWidth(70);
		table.getColumnModel().getColumn(2).setMinWidth(70);
		table.getColumnModel().getColumn(3).setMinWidth(70);
		table.getColumnModel().getColumn(4).setMinWidth(70);
		table.getColumnModel().getColumn(5).setMinWidth(70);
		table.getColumnModel().getColumn(6).setMinWidth(70);
		
		panelBotones = new JPanel();
		panelBotones.setPreferredSize(new Dimension(212, 234));
		panelBotones.setBackground(new Color(128, 255, 128));
		contentPane.add(panelBotones, BorderLayout.EAST);
		SpringLayout sl_panelBotones = new SpringLayout();
		panelBotones.setLayout(sl_panelBotones);
		
		
		
		btnModificar = new JButton("Modificar");
		sl_panelBotones.putConstraint(SpringLayout.NORTH, btnModificar, 11, SpringLayout.NORTH, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.WEST, btnModificar, 10, SpringLayout.WEST, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.SOUTH, btnModificar, 46, SpringLayout.NORTH, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.EAST, btnModificar, -10, SpringLayout.EAST, panelBotones);
		panelBotones.add(btnModificar);
		
		btnModificar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				ModificarDispositivoAlt ventanaModificar = new ModificarDispositivoAlt(api, (Integer) table.getModel().getValueAt(table.getSelectedRow(), 0));
				boolean res = ventanaModificar.mostrar();
				if (res)
					obtenerListaDispositivos();
			
			}
			
		});
		
		btnEliminar = new JButton("Eliminar");
		sl_panelBotones.putConstraint(SpringLayout.NORTH, btnEliminar, 6, SpringLayout.SOUTH, btnModificar);
		sl_panelBotones.putConstraint(SpringLayout.WEST, btnEliminar, 0, SpringLayout.WEST, btnModificar);
		sl_panelBotones.putConstraint(SpringLayout.SOUTH, btnEliminar, 85, SpringLayout.NORTH, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.EAST, btnEliminar, -10, SpringLayout.EAST, panelBotones);
		panelBotones.add(btnEliminar);
		
		btnEliminar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				////////////  ELIMINAR TIPO
				eliminarFilasSeleccionadas();
			}
			
		});
		
		btnVolver = new JButton("Volver");
		sl_panelBotones.putConstraint(SpringLayout.NORTH, btnVolver, -33, SpringLayout.SOUTH, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.WEST, btnVolver, -114, SpringLayout.EAST, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.EAST, btnVolver, -10, SpringLayout.EAST, panelBotones);
		sl_panelBotones.putConstraint(SpringLayout.SOUTH, btnVolver, -10, SpringLayout.SOUTH, panelBotones);
		panelBotones.add(btnVolver);
		
		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		sl_panelBotones.putConstraint(SpringLayout.WEST, progressBar, 0, SpringLayout.WEST, btnModificar);
		sl_panelBotones.putConstraint(SpringLayout.EAST, progressBar, 0, SpringLayout.EAST, btnModificar);
		sl_panelBotones.putConstraint(SpringLayout.SOUTH, progressBar, -6, SpringLayout.NORTH, btnVolver);
		panelBotones.add(progressBar);
		
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cerrar();
			}
		});
		
		limpiarLista();
		////////////    DESHABILITAR BOTONES    /////////////////
		habilitarBotones();
	}
	
	private void cerrar() {
		if(workerRecuperarLista != null)
			workerRecuperarLista.cancel(true);
		this.dispose();
	}
	
	private void obtenerListaDispositivos() {
		table.setEnabled(false);
		progressBar.setIndeterminate(true);
		// Realiza la recuperación de datos en segundo plano.
		(workerRecuperarLista = new RecuperarListaWorker()).execute();
	}
	
	private void limpiarLista() {
		tblmodel.setRowCount(0);
	}
	
	private void habilitarBotones() {
		int filasSeleccionadas = table.getSelectedRowCount();
		boolean unaSolaFilaSeleccionada = filasSeleccionadas == 1;
		// Habilitar o deshabilitar botones en función de
		// la cantidad de filas seleccionadas.
		btnEliminar.setEnabled		(filasSeleccionadas > 0);
		btnModificar.setEnabled		(unaSolaFilaSeleccionada);
	}
	
	private void eliminarFilasSeleccionadas() {
		int[] filas = table.getSelectedRows();
		if (filas.length <= 0) return; // No realizar acción si no se seleccionó nada.
		String mensaje = filas.length > 1 ?
				String.format("Estas seguro de eliminar los %d dispositivos seleccionados?", filas.length) :
				"Estas seguro de eliminar el dispositivo seleccionado?";
		// Ventana de confirmación
		if(JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, mensaje, "Confirmar eliminacion", JOptionPane.YES_NO_OPTION)){
			boolean seCambio = false;
			try {
				// Eliminar los registros
				for(int iFila: filas) {
					api.eliminarDispositivo((Integer) table.getModel().getValueAt(iFila, 0));
					seCambio = true;
				}
			} catch (AppException e) {
				Helpers.mostrarMensajeDeError(this, e, "Error al eliminar");
			}
			finally {
				// Actualizar lista
				if(seCambio)
					obtenerListaDispositivos();
			}
		}
	}
	
	private void onTerminoRecuperarLista(RecuperarListaWorker worker) {
		try {
			List<DispositivoDTO> disp = worker.get();
			limpiarLista();
			// Agrega las tipos en el model
			for (DispositivoDTO d : disp) {
				tblmodel.addRow(new Object[] { d.getCodigo(), d.getNombre(), estadoDispositivo(d.getEstado()), d.getDescripcion(), d.getModelo(), d.getMarca(), d.getNombreEspacio() });
			}
		} catch (CancellationException e) {
			if(!this.isVisible()) {
				System.out.print("Se canceló la recuperación de la lista: ");
				System.out.println(e);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// Obtiene la causa de la excepción que se tiró durante la ejecución del Worker.
			Throwable causa = e.getCause();
			Helpers.mostrarMensajeDeError(this, causa, "Error al recuperar datos");
		}
		finally {
			progressBar.setIndeterminate(false);
			table.setEnabled(true);
		}
	}
	
	private class RecuperarListaWorker extends SwingWorker<List<DispositivoDTO>,Void> {

		@Override
		protected List<DispositivoDTO> doInBackground() throws Exception {
			// Obtiene la lista de tipos a mostrar
			List<DispositivoDTO> disp = api.listarDispositivos(null);
			return disp;
		}
		
		@Override
		protected void done() {
			EventQueue.invokeLater( () -> onTerminoRecuperarLista(this) );
		}
		
	}
	
	// Auxiliares
	private String estadoDispositivo(boolean estado) {
		return estado ? "Activo" : "Inactivo";
	}

}
