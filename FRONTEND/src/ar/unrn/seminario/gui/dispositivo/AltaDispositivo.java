package ar.unrn.seminario.gui.dispositivo;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.unrn.seminario.gui.common.Helpers;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Objects;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class AltaDispositivo extends JDialog {

	protected IApi api;
	protected String tituloMsjError;
	
	private JPanel contentPane, panelBotones;
	protected JTextField txtNombre, txtDescripcion, txtModelo, txtMarca;
	protected JButton btnCrear, btnCancelar;
	private JLabel MarcaLabel, ModeloLabel, EspacioLabel;
	protected JCheckBox ActivoCheckBox;
	protected JComboBox<ItemCodigo> comboBoxEspacio;
	

	/**
	 * Create the frame.
	 */
	public AltaDispositivo(IApi api) {
		this.api = api;
		this.tituloMsjError = "Error al registrar el dispositivo";
		setTitle("Alta Dispositivo");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 436);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(120, 49, 207, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel NombreLabel = new JLabel("Nombre");
		NombreLabel.setBounds(30, 52, 82, 14);
		contentPane.add(NombreLabel);
		
		panelBotones = new JPanel();
		panelBotones.setBackground(new Color(128, 255, 128));
		panelBotones.setBounds(19, 334, 394, 52);
		contentPane.add(panelBotones);
		panelBotones.setLayout(null);
		
		btnCrear = new JButton("Crear");
		
		btnCrear.addActionListener(getAccion());
		
		btnCrear.setBounds(97, 18, 89, 23);
		panelBotones.add(btnCrear);
		
		JButton CancelarButton = new JButton("Cancelar");
		CancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		CancelarButton.setBounds(218, 18, 89, 23);
		panelBotones.add(CancelarButton);
		
		ActivoCheckBox = new JCheckBox("Activo");
		ActivoCheckBox.setBounds(120, 83, 97, 23);
		contentPane.add(ActivoCheckBox);
		
		JLabel EstadoLabel = new JLabel("Estado");
		EstadoLabel.setBounds(30, 87, 82, 14);
		contentPane.add(EstadoLabel);
		
		JLabel DescripcionLabel = new JLabel("Descripción");
		DescripcionLabel.setBounds(30, 121, 82, 14);
		contentPane.add(DescripcionLabel);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(120, 118, 207, 52);
		contentPane.add(txtDescripcion);
		
		txtModelo = new JTextField();
		txtModelo.setColumns(10);
		txtModelo.setBounds(120, 220, 207, 20);
		contentPane.add(txtModelo);
		
		ModeloLabel = new JLabel("Modelo");
		ModeloLabel.setBounds(30, 223, 82, 14);
		contentPane.add(ModeloLabel);
		
		txtMarca = new JTextField();
		txtMarca.setColumns(10);
		txtMarca.setBounds(120, 189, 207, 20);
		contentPane.add(txtMarca);
		
		MarcaLabel = new JLabel("Marca");
		MarcaLabel.setBounds(30, 192, 82, 14);
		contentPane.add(MarcaLabel);
		
		EspacioLabel = new JLabel("Espacio Publico:");
		EspacioLabel.setBounds(30, 262, 82, 14);
		contentPane.add(EspacioLabel);
		
		comboBoxEspacio = new JComboBox<>();
		comboBoxEspacio.setBounds(120, 259, 151, 22);
		contentPane.add(comboBoxEspacio);
		
		// Agregar lista de espacios públicos para seleccionar.
		comboBoxEspacio.addItem( new ItemEspacioVacio("(seleccionar)") );
		for (EspacioPublicoDTO camp : api.listarEspaciosPublicos(null)) {
			comboBoxEspacio.addItem( new ItemEspacioExistente(camp) );
		}
	}
	
	
	protected ActionListener getAccion() {
		return new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(enviar()) dispose();
			}
			
		};
	}

	
	private Boolean obtenerEstadoCheckBox () {
		return ActivoCheckBox.isSelected();
	}

	
	protected boolean enviar() {
		boolean exito = false;
		try {
			// Preparar datos
			String nombre = txtNombre.getText();
			Boolean estado = obtenerEstadoCheckBox();
			String descripcion = txtDescripcion.getText();
			String modelo = txtModelo.getText();
			String marca = txtMarca.getText();

			exito = registrar(nombre, estado, descripcion, modelo, marca, ((ItemCodigo)comboBoxEspacio.getSelectedItem()).getCodigo());
		} catch (DataEmptyException e) {
			String msj = String.format("El campo '%s' está vacío", e.getNombreCampo());
			msj = msjErrorPorCampo.getOrDefault(e.getNombreCampo(), msj);
			JOptionPane.showMessageDialog(this, msj, tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DuplicateException e) {
			JOptionPane.showMessageDialog(this, "Ya existe un espacio público con el mismo nombre.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DataAccessException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "No se pudo acceder a los datos.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (AppException e) {
			//JOptionPane.showMessageDialog(null, e.getMessage(), "Error "+e.getClass().getName(), JOptionPane.ERROR_MESSAGE);
			Helpers.mostrarMensajeDeError(this, e, tituloMsjError);
		}
		if(exito) onEnvioExitoso();
		return exito;
	}
	
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Dispositivo registrado con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	protected boolean registrar(String nombre, Boolean estado, String descripcion, String modelo, String marca, Integer codigoEspacio) {
		api.registrarDispositivo(0, nombre, estado, descripcion, modelo, marca, codigoEspacio);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}
	
	private Map<String,String> msjErrorPorCampo = Map.of(
			"espacio", "No se especificó el espacio público.");

	/** Elemento para JComboBox. */
	abstract class ItemCodigo {
	abstract Integer getCodigo();
	abstract String getNombre();
	public int hashCode() { return Objects.hashCode(getCodigo()); }
	/** @apiNote El método setSelectedItem de JComboBox depende del equals(). */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (this.getClass() != obj.getClass())
			return false;
		ItemCodigo o = (ItemCodigo)obj;
		return this.getCodigo() == o.getCodigo();
	}
}

class ItemDispositivoExistente extends ItemCodigo {
	private final DispositivoDTO dispositivo;
	
	ItemDispositivoExistente(DispositivoDTO dispositivo) {
		this.dispositivo = dispositivo;
	}

	public Integer getCodigo() { return dispositivo.getCodigo(); }
	public String getNombre() { return dispositivo.getNombre(); }
	//public String getEstado() { return dispositivo.getEstado(); }
	//public String getDescripcion() { return dispositivo.getDescripcion(); }
	//public String getModelo() { return dispositivo.getModelo(); }
	//public String getMarca() { return dispositivo.getMarca(); }
	//public Integer getCodigoEspacio() { return dispositivo.getCodigoEspacio(); }
	
	@Override
	public String toString() {
		return String.format("Cód %s: %s", getCodigo(), getNombre());
	}
}

final class ItemDispositivoVacio extends ItemCodigo {
	private String textoAMostrar;
	public ItemDispositivoVacio() { this(null); }
	public ItemDispositivoVacio(String texto) {
		this.textoAMostrar = Objects.requireNonNullElse(texto, "(vacío)");
	}
	public Integer getCodigo() { return null; }
	public String getNombre() { return null; }
	public String toString() { return textoAMostrar; }
}

	
class ItemEspacioExistente extends ItemCodigo {
	private final EspacioPublicoDTO espacio;
	
	ItemEspacioExistente(EspacioPublicoDTO espacio) {
		this.espacio = espacio;
	}

	public Integer getCodigo() { return espacio.getCodigo(); }
	public String getNombre() { return espacio.getNombre(); }
	
	@Override
	public String toString() {
		return String.format("Cód %s: %s", getCodigo(), getNombre());
	}
}

final class ItemEspacioVacio extends ItemCodigo {
	private String textoAMostrar;
	public ItemEspacioVacio() { this(null); }
	public ItemEspacioVacio(String texto) {
		this.textoAMostrar = Objects.requireNonNullElse(texto, "(vacío)");
	}
	public Integer getCodigo() { return null; }
	public String getNombre() { return null; }
	public String toString() { return textoAMostrar; }
}	
	
	
}
