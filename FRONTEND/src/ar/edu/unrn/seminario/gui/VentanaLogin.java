package ar.edu.unrn.seminario.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaLogin extends JFrame {
    private JTextField usuarioField;
    private JPasswordField contrasenaField;

    public VentanaLogin() {
        setTitle("Inicio de Sesión");
        setSize(369, 220);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());

        JPanel panelCampos = new JPanel();
        JLabel usuarioLabel = new JLabel("Usuario:");
        usuarioLabel.setBounds(14, 14, 66, 47);
        JLabel contrasenaLabel = new JLabel("Contraseña:");
        contrasenaLabel.setBounds(14, 64, 84, 33);
        usuarioField = new JTextField(20);
        usuarioField.setBounds(102, 27, 166, 21);
        contrasenaField = new JPasswordField(20);
        contrasenaField.setBounds(102, 70, 166, 21);
        panelCampos.setLayout(null);
        panelCampos.add(usuarioLabel);
        panelCampos.add(usuarioField);
        panelCampos.add(contrasenaLabel);
        panelCampos.add(contrasenaField);

        getContentPane().add(panelCampos, BorderLayout.CENTER);
        JButton botonLogin = new JButton("Login");
        botonLogin.setBounds(153, 107, 57, 21);
        panelCampos.add(botonLogin);
        JButton botonRegistrarse = new JButton("Registrarse");
        botonRegistrarse.setBounds(140, 138, 85, 21);
        panelCampos.add(botonRegistrarse);
        JButton botonSalir = new JButton("Salir");
        botonSalir.setBounds(292, 152, 53, 21);
        panelCampos.add(botonSalir);
        
                // Agrega un ActionListener al botón "Salir"
                botonSalir.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });
        
                // Agrega un ActionListener al botón "Registrarse"
                botonRegistrarse.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // Aquí puedes implementar la funcionalidad para el registro de usuarios
                        // Abre una ventana de registro o realiza alguna acción relacionada
                    }
                });
        
                // Agrega un ActionListener al botón "Login"
                botonLogin.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // Aquí puedes agregar la lógica de autenticación
                        String usuario = usuarioField.getText();
                        char[] contrasena = contrasenaField.getPassword();
                        // Verifica el usuario y la contraseña aquí
                        // Si la autenticación es exitosa, abre la ventana principal
                    }
                });

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                VentanaLogin ventana = new VentanaLogin();
                ventana.setVisible(true);
            }
        });
    }
}