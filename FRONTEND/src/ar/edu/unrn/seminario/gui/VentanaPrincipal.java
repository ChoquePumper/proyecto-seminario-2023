package ar.edu.unrn.seminario.gui;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.MemoryApi;
import ar.edu.unrn.seminario.tipo.AltaTipo;
import ar.edu.unrn.seminario.tipo.ListadoTipos;
import ar.unrn.seminario.gui.*;
import ar.unrn.seminario.gui.campana.AltaCampana;
import ar.unrn.seminario.gui.campana.ListadoCampanas;
import ar.unrn.seminario.gui.dispositivo.AltaDispositivo;
import ar.unrn.seminario.gui.dispositivo.ListadoDispositivos;
import ar.unrn.seminario.gui.espaciopublico.AltaEspacioPublico;
import ar.unrn.seminario.gui.espaciopublico.ListadoEspaciosPublicos;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IApi api = new MemoryApi();
					VentanaPrincipal frame = new VentanaPrincipal(api);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal(IApi api) {
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu usuarioMenu = new JMenu("Usuarios");
		menuBar.add(usuarioMenu);

		JMenuItem altaUsuarioMenuItem = new JMenuItem("Alta/Modificaci\u00F3n");
		altaUsuarioMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				AltaUsuario alta = new AltaUsuario(api);
				alta.setLocationRelativeTo(null);
				alta.setVisible(true);
			}
			
		});
		usuarioMenu.add(altaUsuarioMenuItem);

		JMenuItem listadoUsuarioMenuItem = new JMenuItem("Listado");
		listadoUsuarioMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				ListadoUsuario listado= new ListadoUsuario(api);
				listado.setLocationRelativeTo(null);
				listado.setVisible(true);
			}
			
		});
		usuarioMenu.add(listadoUsuarioMenuItem);
		
		JMenu campanaMenu = new JMenu("Campañas");
		menuBar.add(campanaMenu);
		
		JMenuItem altaCampanaMenuItem = new JMenuItem("Alta ");
		campanaMenu.add(altaCampanaMenuItem);
		
		altaCampanaMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				AltaCampana alta= new AltaCampana(api);
				alta.setLocationRelativeTo(null);
				alta.setVisible(true);
			}
			
		});
		
		JMenuItem listadoModificacionCampanaMenuItem = new JMenuItem("Listado/Modificación");
		campanaMenu.add(listadoModificacionCampanaMenuItem);
		
		listadoModificacionCampanaMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				ListadoCampanas listado= new ListadoCampanas(api);
				listado.setLocationRelativeTo(null);
				listado.setVisible(true);
			}
			
		});
		
		
		JMenu espaciosMenu = new JMenu("Espacios");
		menuBar.add(espaciosMenu);
		
		JMenuItem altaEspacioMenuItem = new JMenuItem("Alta");
		espaciosMenu.add(altaEspacioMenuItem);
		
		altaEspacioMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				AltaEspacioPublico alta = new AltaEspacioPublico(api);
				alta.setLocationRelativeTo(null);
				alta.setVisible(true);
			}
			
		});
		
		JMenuItem listadoModificacionEspacioMenuItem = new JMenuItem("Listado/Modificación");
		espaciosMenu.add(listadoModificacionEspacioMenuItem);
		
		listadoModificacionEspacioMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				ListadoEspaciosPublicos lista = new ListadoEspaciosPublicos(api);
				lista.setLocationRelativeTo(null);
				lista.setVisible(true);
			}
			
		});
		
		JMenu tiposMenu = new JMenu("Tipos");
		menuBar.add(tiposMenu);
		
		JMenuItem altaTipoMenuItem = new JMenuItem("Alta");
		altaTipoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaTipo altaTipo = new AltaTipo(api);
				altaTipo.setLocationRelativeTo(null);
				altaTipo.setVisible(true);
			}
		});
		tiposMenu.add(altaTipoMenuItem);
		
		JMenuItem listadoModificacionTiposMenuItem = new JMenuItem("Listado/Modificación");
		listadoModificacionTiposMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListadoTipos listaTipo = new ListadoTipos(api);
				listaTipo.setLocationRelativeTo(null);
				listaTipo.setVisible(true);
			}
		});
		tiposMenu.add(listadoModificacionTiposMenuItem);
		
		JMenu dispositivosMenu = new JMenu("Dispositivos");
		menuBar.add(dispositivosMenu);
		
		JMenuItem altaDispositivoMenuItem = new JMenuItem("Alta");
		altaDispositivoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaDispositivo altaDispositivo = new AltaDispositivo(api);
				altaDispositivo.setLocationRelativeTo(null);
				altaDispositivo.setVisible(true);
			}
		});
		dispositivosMenu.add(altaDispositivoMenuItem);
		
		JMenuItem ListadoModificacionDispositivoMenuItem = new JMenuItem("Listado/Modificación");
		ListadoModificacionDispositivoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListadoDispositivos listaDisp = new ListadoDispositivos(api);
				listaDisp.setLocationRelativeTo(null);
				listaDisp.setVisible(true);
			}
		});
		dispositivosMenu.add(ListadoModificacionDispositivoMenuItem);

		JMenu configuracionMenu = new JMenu("Configuraci\u00F3n");
		menuBar.add(configuracionMenu);

		JMenuItem salirMenuItem = new JMenuItem("Salir");
		salirMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		configuracionMenu.add(salirMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
