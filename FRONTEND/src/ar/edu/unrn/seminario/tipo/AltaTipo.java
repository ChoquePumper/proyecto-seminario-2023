package ar.edu.unrn.seminario.tipo;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.unrn.seminario.gui.common.Helpers;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Date;
import java.awt.event.ActionEvent;

public class AltaTipo extends JDialog {

	protected IApi api;
	protected String tituloMsjError;
	
	private JPanel contentPane, panelBotones;
	protected JTextField txtNombre;
	protected JButton btnCrear, btnCancelar;
	

	/**
	 * Create the frame.
	 */
	public AltaTipo(IApi api) {
		this.api = api;
		this.tituloMsjError = "Error al registrar el tipo de espacio público";
		setTitle("Alta Tipo");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 204);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(120, 49, 207, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel NombreLabel = new JLabel("Nombre tipo:");
		NombreLabel.setBounds(30, 52, 82, 14);
		contentPane.add(NombreLabel);
		
		panelBotones = new JPanel();
		panelBotones.setBackground(new Color(128, 255, 128));
		panelBotones.setBounds(18, 102, 394, 52);
		contentPane.add(panelBotones);
		panelBotones.setLayout(null);
		
		btnCrear = new JButton("Crear");
		
		btnCrear.addActionListener(getAccion());
		
		btnCrear.setBounds(97, 18, 89, 23);
		panelBotones.add(btnCrear);
		
		JButton CancelarButton = new JButton("Cancelar");
		CancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		CancelarButton.setBounds(218, 18, 89, 23);
		panelBotones.add(CancelarButton);
	}
	
	
	protected ActionListener getAccion() {
		return new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(enviar()) dispose();
			}
			
		};
	}
	
	protected boolean enviar() {
		boolean exito = false;
		try {
			// Preparar datos
			String nombre = txtNombre.getText();

			exito = registrar(nombre);
		} catch (DuplicateException e) {
			JOptionPane.showMessageDialog(this, "Ya existe un tipo de espacio público con el mismo nombre.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (DataAccessException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "No se pudo acceder a los datos.", tituloMsjError, JOptionPane.ERROR_MESSAGE);
		} catch (AppException e) {
			//JOptionPane.showMessageDialog(null, e.getMessage(), "Error "+e.getClass().getName(), JOptionPane.ERROR_MESSAGE);
			Helpers.mostrarMensajeDeError(this, e, tituloMsjError);
		}
		if(exito) onEnvioExitoso();
		return exito;
	}
	
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Tipo registrado con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	protected boolean registrar(String nombre) {
		api.registrarTipo(0, nombre);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}
	
}
