package ar.edu.unrn.seminario.tipo;

import java.awt.EventQueue;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;

public class ModificarTipoAlt extends AltaTipo {

	private boolean resultado = false;
	private TipoDTO tipo;
	public ModificarTipoAlt(IApi api, Integer codigoTipo) {
		super(api);

		this.tituloMsjError = "Error al modificar el tipo de espacio público";
		this.tipo = api.obtenerTipoPorCodigo(codigoTipo);
		
		// Retocar la GUI.
		setTitle("                     Modificar Tipo");
		btnCrear.setText("Modificar");

		txtNombre.setText(tipo.getNombre());
	
	}
	
	public boolean mostrar() {
		this.setModal(true);
		this.setVisible(true);
		return resultado;
	}
	
	@Override
	protected boolean enviar() {
		resultado = super.enviar();
		return resultado;
	}
	
	@Override
	protected void onEnvioExitoso() {
		JOptionPane.showMessageDialog(null, "Tipo modificado con exito!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	@Override
	protected boolean registrar(String nombre) {
		api.modificarTipo(tipo.getCodigo(), nombre);
		// No llegará aquí si lo anterior tira excepción.
		return true;
	}


}
