package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;

public class Periodo {
	private Integer codigo;
	private String nombre;
	private LocalDate fechInicio;
	private LocalDate fechaFin;
	private String descripcion;
	
	public Periodo(Integer codigo, String nombre, LocalDate fechInicio, LocalDate fechaFin, String descripcion) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.fechInicio = fechInicio;
		this.fechaFin = fechaFin;
		this.descripcion = descripcion;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechInicio() {
		return fechInicio;
	}

	public void setFechInicio(LocalDate fechInicio) {
		this.fechInicio = fechInicio;
	}

	public LocalDate getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(LocalDate fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
