package ar.edu.unrn.seminario.modelo;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;

import static ar.edu.unrn.seminario.commons.Validaciones.*;

/**
 * <p>Representa un espacio público. Su ubicación se determina por una {@code ZonaDelimitada}.
 * </p>
 * <p>Éste puede estar asociado a una o más campañas.</p>
 * 
 * @author Grupo 5
 */
public class EspacioPublico {
	private Integer codigo;
	private String nombre;
	private String direccion;
	private ZonaDelimitada zonaDelimitada;
	private Tipo tipo;
	private String descripcion;
	private String urlImagen;
	private String qr;
	private Campana campana;
	
	public EspacioPublico(Integer codigo, String nombre, String direccion, ZonaDelimitada zonaDelimitada, Tipo tipo, String descripcion, String urlImagen, String qr,
			Campana campana) throws DataEmptyException {
		
		setCodigo(codigo);
		setNombre(nombre);
		setDireccion(direccion);
		setZonaDelimitada(zonaDelimitada);
		setTipo(tipo);
		setDescripcion(descripcion);
		setUrlImagen(urlImagen);
		setQr(qr);
		setCampana(campana);
		
	}
	
	public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = validarNoNulo(codigo, "codigo");
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = validarDato(nombre, "nombre");;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = validarDato(direccion, "direccion");
    }

   public ZonaDelimitada getZonaDelimitada() {
        return zonaDelimitada;
    }

    public void setZonaDelimitada(ZonaDelimitada zonaDelimitada) {
        this.zonaDelimitada = validarNoNulo(zonaDelimitada, "zonaDelimitada");
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = validarNoNulo(tipo, "tipo");
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = validarDato(descripcion, "descripcion");
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

   public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public Campana getCampana() {
        return campana;
    }

    public void setCampana(Campana campana) {
        this.campana = validarNoNulo(campana, "campana");
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EspacioPublico other = (EspacioPublico) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
