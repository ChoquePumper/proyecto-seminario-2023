package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;
import static ar.edu.unrn.seminario.commons.Validaciones.*;

/**
 * Es una campaña que está asociada a uno o más de un {@code EspacioPublico} y
 * a un {@code Periodo} para definir la duración.
 * 
 * @author Grupo 5
 */
public class Campana { // Campaña
	private Integer codigo;
	private LocalDate fechaEdicion;
	private List<Periodo> temporadas;
	private String nombre;
	private String descripcion;
	
	public Campana(Integer codigo, LocalDate fechaEdicion, String nombre, String descripcion, List<Periodo> temporadas) throws DataEmptyException {
		this(codigo, fechaEdicion, nombre, descripcion);
		setTemporadas(temporadas);
	}
	
	public Campana(Integer codigo, LocalDate fechaEdicion, String nombre, String descripcion) throws DataEmptyException {
		super();
		
		setCodigo(codigo);
		setFechaEdicion(fechaEdicion);
		setNombre(nombre);
		setDescripcion(descripcion);
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = validarNoNulo(codigo, "codigo");
	}

	public LocalDate getFechaEdicion() {
		return fechaEdicion;
	}
	
	public void setFechaEdicion(LocalDate fechaEdicion) {
		this.fechaEdicion = validarNoNulo(fechaEdicion, "fecha");
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = validarDato(nombre, "nombre");
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = validarNoNulo(descripcion, "descripcion");
	}
	
	public List<Periodo> getTemporadas() {
		return temporadas;
	}
	
	public void setTemporadas(List<Periodo> temporadas) {
		this.temporadas = new ArrayList<>(temporadas);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campana other = (Campana) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	
}
