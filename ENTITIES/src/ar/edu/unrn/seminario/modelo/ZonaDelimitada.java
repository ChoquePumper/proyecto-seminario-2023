package ar.edu.unrn.seminario.modelo;

import java.util.Objects;

import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.NegativeException;

/**
 * Clase {@code ZonaDelimitada}<br>
 * Marca una zona por sus coordenadas (<u>latitud</u> y <u>longitud</u>) y
 * un <u>radio</u>.
 * 
 * Nota: confirmar en que unidad de medida será el radio.
 *
 * @author Grupo 5
 */
public class ZonaDelimitada {

	private double latitud;
	private double longitud;
	private double radio;

	public ZonaDelimitada(Double latitud, Double longitud, Double radio) throws NegativeException{
		
		if (esDatoVacio(latitud))
			throw new DataEmptyException("latitud");
		
		if (esDatoVacio(longitud))
			throw new DataEmptyException("longitud");
		
		if (esDatoVacio(radio))
			throw new DataEmptyException("radio");
		
		
		if (esDatoNegativo(radio))
			throw new NegativeException("radio");
		
		this.latitud = latitud;
		this.longitud = longitud;
		this.radio = radio;
	}

	public double getLatitud() {
	    return latitud;
	}

	public void setLatitud(double latitud) {
		if (esDatoVacio(latitud))
			throw new DataEmptyException("latitud");
	    this.latitud = latitud;
	}

	public double getLongitud() {
	    return longitud;
	}

	public void setLongitud(double longitud) {
		if (esDatoVacio(longitud))
			throw new DataEmptyException("longitud");
	    this.longitud = longitud;
	}

	public double getRadio() {
	    return radio;
	}

	public void setRadio(double radio) {
		if (esDatoVacio(radio))
			throw new DataEmptyException("radio");
	    this.radio = radio;
	}
	
	private boolean esDatoVacio(Double dato) {
		return dato.equals(null);
	}
	
	private boolean esDatoNegativo(double dato) {
		if(dato < 0)
			return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		// Genera un hash con los 3 campos.
		return Objects.hash(latitud, longitud, radio);
	}
	
	@Override
	public boolean equals(Object otro) {
		// Compara si el otro objeto es instanacia de la misma clase
		if (! (otro instanceof ZonaDelimitada))
			return false;
		// Compara todos los campos
		ZonaDelimitada otraZona = (ZonaDelimitada)otro;
		return this.getLatitud() == otraZona.getLatitud() &&
				this.getLongitud() == otraZona.getLongitud() &&
				this.getRadio() == otraZona.getRadio();
	}
}
