package ar.edu.unrn.seminario.modelo;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Tipo {
	
	private Integer codigo;
	private String nombre;
	
	
	public Tipo(Integer codigo, String nombre) throws DataEmptyException {
		
		if (esDatoVacio(codigo))
			throw new DataEmptyException("codigo");
		
		if (esDatoVacio(nombre))
			throw new DataEmptyException("nombre");
		
		
		this.codigo = codigo;
		this.nombre = nombre;
	
	}
	
	private boolean esDatoVacio(String dato) {
		return dato.isEmpty();
	}
	
	private boolean esDatoVacio(Integer dato) {
		return dato.equals(null);
	}
	
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tipo other = (Tipo) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

}
