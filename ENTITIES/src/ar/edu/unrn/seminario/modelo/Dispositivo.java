package ar.edu.unrn.seminario.modelo;

import static ar.edu.unrn.seminario.commons.Validaciones.*;

public class Dispositivo {
	private Integer codigo;
	private String nombre;
	private Boolean estado;
	private String descripcion;
	private String modelo;
	private String marca;
	private EspacioPublico espacio;
	
	public Dispositivo(Integer codigo, String nombre, Boolean estado, String descripcion, String modelo, String marca, EspacioPublico espacio) {
		setCodigo(codigo);
		setEstado(estado);
		setNombre(nombre);
		setDescripcion(descripcion);
		setModelo(modelo);
		setMarca(marca);
		setEspacio(espacio);
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = validarNoNulo(codigo, "codigo");
	}

	public Boolean estaActivo() {
		return estado;
	}

	private void setEstado(Boolean estado) {
		this.estado = validarNoNulo(estado, "estado");
	}

	public void activar() {
		this.estado = true;
	}
	public void desactivar() {
		this.estado = false;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = validarDato(nombre, "nombre");
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = validarDato(descripcion, "descripcion");
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = validarDato(modelo, "modelo");
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = validarDato(marca, "marca");
	}
	
	public EspacioPublico getEspacio() {
		return espacio;
	}

	public void setEspacio(EspacioPublico espacio) {
		this.espacio = validarNoNulo(espacio, "espacio");
	}

	
}
