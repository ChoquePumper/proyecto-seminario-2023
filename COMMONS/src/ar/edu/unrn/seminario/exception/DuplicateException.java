package ar.edu.unrn.seminario.exception;

public class DuplicateException extends AppException {
		
	public DuplicateException (String message) {
		super(message);
	}
	
	public static DuplicateException porDato(String dato) {
		return new DuplicateException("El dato '" + dato + "' ya existe.");
	}
		
}
