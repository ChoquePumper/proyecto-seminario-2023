package ar.edu.unrn.seminario.exception;

public class NegativeException extends DataInvalidException	{

	public NegativeException(String campo) {
		this("El campo '" + campo + "' no puede tomar valores negativos.", campo);
	}
	
	public NegativeException(String campo, String mensaje) {
		super(campo, mensaje);
	}

}
