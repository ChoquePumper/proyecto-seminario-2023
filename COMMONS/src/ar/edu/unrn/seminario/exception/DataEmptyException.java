package ar.edu.unrn.seminario.exception;

public class DataEmptyException extends DataInvalidException {

	public DataEmptyException(String campo) {
		this(campo, "El campo '" + campo + "', no puede estar vacío.");
	}
	
	public DataEmptyException(String campo, String mensaje) {
		super(campo, mensaje);
	}

}
