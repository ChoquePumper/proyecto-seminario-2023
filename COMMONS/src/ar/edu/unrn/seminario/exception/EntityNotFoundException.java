package ar.edu.unrn.seminario.exception;

public class EntityNotFoundException extends AppException {
	
	public EntityNotFoundException (String message) {
		super( "No se encuentra el objeto '" + message + "'");
	}
	
}
