package ar.edu.unrn.seminario.exception;

public class IntegrityException extends AppException {

	public IntegrityException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IntegrityException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IntegrityException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
