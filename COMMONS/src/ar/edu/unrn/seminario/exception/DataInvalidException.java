package ar.edu.unrn.seminario.exception;

public class DataInvalidException extends AppException {

	protected String nombreCampo;
	
	public DataInvalidException(String campo, String mensaje) {
		super(mensaje);
		nombreCampo = campo;
	}
	
	public String getNombreCampo() {
		return nombreCampo;
	}
}
