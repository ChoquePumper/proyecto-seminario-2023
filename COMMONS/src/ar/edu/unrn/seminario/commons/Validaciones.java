package ar.edu.unrn.seminario.commons;

import ar.edu.unrn.seminario.exception.DataEmptyException;

// Clase con algunos métodos estáticos comunes para validaciones.
// Las clases pueden tener sus propias formas de validación.
public class Validaciones {
	
	/** Comprueba si el objeto es nulo. */
	public static <T> T validarNoNulo(T campo, String nombre) throws DataEmptyException {
		if (campo == null)
			throw new DataEmptyException(nombre);
		return campo;
	}
	
	/** Para String, solo comprueba si es vacío. */
	public static String validarDato(String campo, String nombre) throws DataEmptyException {
		if (esDatoVacio(validarNoNulo(campo,nombre)))
			throw new DataEmptyException(nombre);
		return campo;
	}
	
	public static boolean esDatoVacio(String dato) {
		return dato.isEmpty();
	}
}
