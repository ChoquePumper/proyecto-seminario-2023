package ar.edu.unrn.seminario.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.modelo.Campana;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.EspacioPublico;
import ar.edu.unrn.seminario.modelo.Tipo;
import ar.edu.unrn.seminario.modelo.ZonaDelimitada;

public interface IApi {

	void registrarUsuario(String usuario, String contrasena, String email, String nombre, Integer codigoRol);

	UsuarioDTO obtenerUsuario(String usuario);

	void eliminarUsuario(String usuario);

	List<RolDTO> obtenerRoles();

	List<RolDTO> obtenerRolesActivos();

	void guardarRol(Integer codigoRol, String descripcion, boolean estado); // crear el objeto de dominio �Rol�

	RolDTO obtenerRolPorCodigo(Integer codigoRol); // recuperar el rol almacenado

	void activarRol(Integer codigoRol); // recuperar el objeto Rol, implementar el comportamiento de estado.

	void desactivarRol(Integer codigoRol); // recuperar el objeto Rol, imp

	List<UsuarioDTO> obtenerUsuarios(); // recuperar todos los usuarios

	void activarUsuario(String usuario); // recuperar el objeto Usuario, implementar el comportamiento de estado.

	void desactivarUsuario(String usuario); // recuperar el objeto Usuario, implementar el comportamiento de estado.

	/*****************************************************************
	 ** SUBDOMINIO - Grupo 5                                        **
	 *****************************************************************/

	// Hola ;)

	// Estas clases solo están aquí como relleno para este fragmento de código.
	// TODO: quitar estas lineas y crear las clases como corresponden después.
	
	//class CampañaDTO {};
	//class EspacioPublicoDTO {};
	//class Dispositivo {}; // TODO: consultar con el otro grupo.
	
	// Requerimientos.
	// Para empezar puse estos tomados del "mockup" (creo que se escribe así)
	// de la interfaz gráfica creada en draw.io y compartida en Drive.

	// Nota: Reemplazar evitar usar la "ñ" u otros caracteres especiales en
	// el código.

	// Campaña
	
	void registrarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion);
	
	void modificarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion); // mismos que crearCampaña?
	
	void eliminarCampana(Integer codigoCampana);

	List<CampanaDTO> listarCampanas(Map<String,Filtro> filtros, Ordenado campoAOrdenar);
	default List<CampanaDTO> listarCampanas(Map<String,Filtro> filtros) {
		return listarCampanas(filtros, null);
	}
	
	CampanaDTO obtenerCampanaPorCodigo(Integer codigoCampana);
	
	List<EspacioPublicoDTO> listarEspaciosAsociados(Integer codigoCampana);
	
	
	// EspacioPúblico
	
	void registrarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion,  double longitud, double latitud, double radio, Integer codigoTipo,
			String descripcion, String urlImagen, String qr, Integer codigoCampana);
	
	void modificarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion, double longitud, double latitud, double radio, Integer codigoTipo,
			String descripcion, String urlImagen, String qr, Integer codigoCampana); // mismos que crearEspacioPublico?
	
	void eliminarEspacioPublico(Integer codigoEspacioPublico);
	
	EspacioPublicoDTO obtenerEspacioPublicoPorCodigo(Integer codigoEspacioPublico);	

	List<EspacioPublicoDTO> listarEspaciosPublicos(Map<String,Filtro> filtros, Ordenado campoAOrdenar);
	default List<EspacioPublicoDTO> listarEspaciosPublicos(Map<String,Filtro> filtros) {
		return listarEspaciosPublicos(filtros, null);
	}
	
	
	// Tipo
	void registrarTipo(Integer codigoTipo, String nombreTipo); 
	void modificarTipo(Integer codigoTipo, String nombreTipo);
	void eliminarTipo(Integer codigoTipo);
	TipoDTO obtenerTipoPorCodigo(Integer codigoTipo); 
	List<TipoDTO> listarTipos();
	
	
	
	void exportarCampanas(String... ids);
	void exportarEspaciosPublicos(String... ids);
	// Qué más?

	// Dispositivo
	void registrarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo, String descripcionDispositivo, String modeloDispositivo, String marcaDispositivo, Integer espacioPublico);
	void modificarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo, String descripcionDispositivo, String modeloDispositivo, String marcaDispositivo, Integer espacioPublico);
	void eliminarDispositivo(Integer codigoDispositivo);
	List<DispositivoDTO> listarDispositivosDeUnEspacio(Integer codigoEspacioPublico);
	List<DispositivoDTO> listarDispositivos(Map<String,Filtro> filtros);
	List<EspacioPublicoDTO> listarEspacioAsociadoDispositivos(Integer codigoEspacioPublico);
	DispositivoDTO obtenerDispositivoPorCodigo(Integer codigoDispositivo);
	//CORREGIR EL JOPTION EN FRONT CON LAS EXCEPTIONS
	
}
