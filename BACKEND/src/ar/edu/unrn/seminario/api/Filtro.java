package ar.edu.unrn.seminario.api;

public interface Filtro {
	
	boolean evaluar(Object o1);
	
	default Filtro y(Filtro otro) {
		return (o1) -> evaluar(o1) && otro.evaluar(o1);
	}
	
	public static Filtro no(Filtro otro) {
		return (o1) -> !otro.evaluar(o1);
	}

	public static Filtro es(Object o2) {
		return (o1) -> o1.equals(o2);
	}

	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> Filtro esMenorA(T o2) {
		return (o1) -> {
			try {
				return ((Comparable<T>) o1).compareTo(o2) < 0;
			} catch (ClassCastException e) {
				throw new RuntimeException("Filtro inválido: se esperaba comparable.");
			}
		};
	}
	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> Filtro esMenorOIgualA(T o2) {
		return (o1) -> {
			try {
				return ((Comparable<T>) o1).compareTo(o2) <= 0;
			} catch (ClassCastException e) {
				throw new RuntimeException("Filtro inválido: se esperaba comparable.");
			}
		};
	}
	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> Filtro esMayorA(T o2) {
		return (o1) -> {
			try {
				return ((Comparable<T>) o1).compareTo(o2) > 0;
			} catch (ClassCastException e) {
				throw new RuntimeException("Filtro inválido: se esperaba comparable.");
			}
		};
	}
	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> Filtro esMayorOIgualA(T o2) {
		return (o1) -> {
			try {
				return ((Comparable<T>) o1).compareTo(o2) >= 0;
			} catch (ClassCastException e) {
				throw new RuntimeException("Filtro inválido: se esperaba comparable.");
			}
		};
	}

	public static Filtro contiene(String o2) {
		return (o1) -> {
			if(o1 instanceof String)
				return ((String) o1).toLowerCase().contains(o2.toLowerCase());
			throw new RuntimeException("Filtro inválido: se esperaba texto.");
		};
	}
	public static Filtro comienzaCon(String o2) {
		return (o1) -> {
			if(o1 instanceof String)
				return ((String) o1).toLowerCase().startsWith(o2.toLowerCase());
			throw new RuntimeException("Filtro inválido: se esperaba texto.");
		};
	}
	public static Filtro terminaCon(String o2) {
		return (o1) -> {
			if(o1 instanceof String)
				return ((String) o1).toLowerCase().endsWith(o2.toLowerCase());
			throw new RuntimeException("Filtro inválido: se esperaba texto.");
		};
	}
	
}
