package ar.edu.unrn.seminario.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ar.edu.unrn.seminario.accesos.CampanaDAOJDBC;
import ar.edu.unrn.seminario.accesos.DispositivoDAOJBDC;
import ar.edu.unrn.seminario.accesos.EspacioPublicoDAOJDBC;
import ar.edu.unrn.seminario.accesos.ObjetoDAO;
import ar.edu.unrn.seminario.accesos.ObjetoJDBC;
import ar.edu.unrn.seminario.accesos.TipoDAOJDBC;
import ar.edu.unrn.seminario.commons.Validaciones;
import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.EntityNotFoundException;
import ar.edu.unrn.seminario.modelo.Campana;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.EspacioPublico;
import ar.edu.unrn.seminario.modelo.Tipo;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.ZonaDelimitada;

public class PersistenceApi implements IApi {
	private String subprotocolo;
	private String subnombre;
	
	// DAOs
	private ObjetoDAO<Campana> campanaDAO;
	private ObjetoDAO<EspacioPublico> espacioPublicoDAO;
	private ObjetoDAO<Dispositivo> dispositivoDAO;
	private ObjetoDAO<Tipo> tipoDAO;
	
	/**
	 * @see ObjetoJDBC#ObjetoJDBC(String,String)
	 */
	public PersistenceApi(String subprotocolo, String subnombre) {
		this.subprotocolo = Validaciones.validarDato(subprotocolo, "subprotocolo");
		this.subnombre = Validaciones.validarDato(subnombre, "subnombre");
		// Crear DAOs JDBC
		this.campanaDAO = new CampanaDAOJDBC(this.subprotocolo, this.subnombre);
		this.tipoDAO = new TipoDAOJDBC(this.subprotocolo, this.subnombre);
		this.espacioPublicoDAO = new EspacioPublicoDAOJDBC(this.subprotocolo, this.subnombre);
		this.dispositivoDAO = new DispositivoDAOJBDC(this.subprotocolo, this.subnombre);
	}
	

	@Override
	public void registrarUsuario(String usuario, String contrasena, String email, String nombre, Integer codigoRol) {
		// TODO Auto-generated method stub

	}

	@Override
	public UsuarioDTO obtenerUsuario(String usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminarUsuario(String usuario) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<RolDTO> obtenerRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RolDTO> obtenerRolesActivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarRol(Integer codigoRol, String descripcion, boolean estado) {
		// TODO Auto-generated method stub

	}

	@Override
	public RolDTO obtenerRolPorCodigo(Integer codigoRol) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void activarRol(Integer codigoRol) {
		// TODO Auto-generated method stub

	}

	@Override
	public void desactivarRol(Integer codigoRol) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void activarUsuario(String usuario) {
		// TODO Auto-generated method stub

	}

	@Override
	public void desactivarUsuario(String usuario) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registrarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion) {
		Campana campanaNueva = new Campana(0, fecha, nombre, descripcion);
		campanaDAO.create(campanaNueva);
		
	}

	@Override
	public void modificarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion) {
		Campana campanaNueva = new Campana(codigoCampana, fecha, nombre, descripcion);
		campanaDAO.update(campanaNueva);
	}

	@Override
	public void eliminarCampana(Integer codigoCampana) {
		campanaDAO.remove(intALong(codigoCampana));
	}

	@Override
	public List<CampanaDTO> listarCampanas(Map<String,Filtro> filtros, Ordenado campoAOrdenar) {
		comprobarFiltrosEntidad(paraFiltrarCampana, paraProbarFiltrarCampana, filtros);
		comprobarOrdenadoEntidad(paraOrdenarCampana, campoAOrdenar);
		return campanaDAO.findAll().stream()
				.filter(aplicarFiltrosParaEntidad(paraFiltrarCampana, filtros))
				.sorted(aplicarOrdenadoParaEntidad(paraOrdenarCampana, campoAOrdenar))
				.map(this::campanaADTO)
				.collect(Collectors.toList());
	}

	@Override
	public CampanaDTO obtenerCampanaPorCodigo(Integer codigoCampana) {
		Campana campana = getCampana(codigoCampana);
		return campanaADTO(campana);
	}

	@Override
	public List<EspacioPublicoDTO> listarEspaciosAsociados(Integer codigoCampana) {
		// TODO Auto-generated method stub
		List<EspacioPublico> lista = espacioPublicoDAO.findAll();
		List<EspacioPublicoDTO> espaciosAsoc = new ArrayList<>();
		for (EspacioPublico ep : lista) {
			if (ep.getCampana().equals(getCampana(codigoCampana)))
				espaciosAsoc.add(espacioADTO(ep));
		}
		
		return espaciosAsoc;
	}

	@Override
	public void registrarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion, double longitud,
			double latitud, double radio, Integer codigoTipo, String descripcion, String urlImagen, String qr,
			Integer codigoCampana) {
		ZonaDelimitada zona = new ZonaDelimitada(latitud, longitud, radio);
		// Tirar excepción si no se especificó una campaña o tipo de espacio público
		if (codigoCampana == null)
			throw new DataEmptyException("campaña", "Se requiere especificar una campaña.");
		if (codigoTipo == null)
			throw new DataEmptyException("tipo", "Se requiere especificar un tipo de espacio público.");
		EspacioPublico espacioNuevo = new EspacioPublico(0, nombre, direccion, zona, getTipo(codigoTipo),
				descripcion, urlImagen, qr, getCampana(codigoCampana));
		espacioPublicoDAO.create(espacioNuevo);
		
	}

	@Override
	public void modificarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion, double longitud,
			double latitud, double radio, Integer codigoTipo, String descripcion, String urlImagen, String qr,
			Integer codigoCampana) {
		ZonaDelimitada zona = new ZonaDelimitada(latitud, longitud, radio);
		// Tirar excepción si no se especificó una campaña o tipo de espacio público
		if (codigoCampana == null)
			throw new DataEmptyException("campaña", "Se requiere especificar una campaña.");
		if (codigoTipo == null)
			throw new DataEmptyException("tipo", "Se requiere especificar un tipo de espacio público.");
		EspacioPublico espacioNuevo = new EspacioPublico(codigoEspacioPublico, nombre, direccion, zona, getTipo(codigoTipo),
				descripcion, urlImagen, qr, getCampana(codigoCampana));
		espacioPublicoDAO.update(espacioNuevo);
	}

	@Override
	public void eliminarEspacioPublico(Integer codigoEspacioPublico) {
		espacioPublicoDAO.remove(intALong(codigoEspacioPublico));
	}

	@Override
	public EspacioPublicoDTO obtenerEspacioPublicoPorCodigo(Integer codigoEspacioPublico) {
		// TODO Auto-generated method stub
		EspacioPublico espacio = getEspacioPublico(codigoEspacioPublico);
		return espacioADTO(espacio);
	}

	@Override
	public List<EspacioPublicoDTO> listarEspaciosPublicos(Map<String,Filtro> filtros, Ordenado campoAOrdenar) {
		comprobarFiltrosEntidad(paraFiltrarEspacio, paraProbarFiltrarEspacio, filtros);
		comprobarOrdenadoEntidad(paraOrdenarEspacio, campoAOrdenar);
		return espacioPublicoDAO.findAll().stream()
				.filter( aplicarFiltrosParaEntidad(paraFiltrarEspacio, filtros) )
				.sorted( aplicarOrdenadoParaEntidad(paraOrdenarEspacio, campoAOrdenar) )
				.map(this::espacioADTO)
				.collect(Collectors.toList());
	}

	@Override
	public void registrarTipo(Integer codigoTipo, String nombreTipo) {
		Tipo tipoNuevo = new Tipo(codigoTipo, nombreTipo);
		tipoDAO.create(tipoNuevo);

	}

	@Override
	public void modificarTipo(Integer codigoTipo, String nombreTipo) {
		Tipo tipoNuevo = new Tipo(codigoTipo, nombreTipo);
		tipoDAO.update(tipoNuevo);

	}

	@Override
	public void eliminarTipo(Integer codigoTipo) {
		tipoDAO.remove(intALong(codigoTipo));

	}

	@Override
	public TipoDTO obtenerTipoPorCodigo(Integer codigoTipo) {
		return tipoADTO(getTipo(codigoTipo));
	}

	@Override
	public List<TipoDTO> listarTipos() {
		return tipoDAO.findAll().stream()
				.map(this::tipoADTO)
				.collect(Collectors.toList());
	}

	@Override
	public void exportarCampanas(String... ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public void exportarEspaciosPublicos(String... ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registrarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo,
			String descripcionDispositivo, String modeloDispositivo, String marcaDispositivo,
			Integer codigoEspacioPublico) {
		// Tirar excepción si no se especificó un tipo de espacio público
		if (codigoEspacioPublico == null)
			throw new DataEmptyException("espacio", "Se requiere especificar un espacio público.");
		Dispositivo dispositivoNuevo = new Dispositivo(codigoDispositivo, nombreDispositivo, estadoDispositivo, 
				descripcionDispositivo, modeloDispositivo, marcaDispositivo, getEspacioPublico(codigoEspacioPublico));
		dispositivoDAO.create(dispositivoNuevo);

	}
	
	@Override
	public void modificarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo, 
			String descripcionDispositivo, String modeloDispositivo, String marcaDispositivo, Integer codigoEspacioPublico) {
		// Tirar excepción si no se especificó un tipo de espacio público
		if (codigoEspacioPublico == null)
			throw new DataEmptyException("espacio", "Se requiere especificar un espacio público.");
		Dispositivo dispositivoModificado = new Dispositivo(codigoDispositivo, nombreDispositivo, estadoDispositivo, 
				descripcionDispositivo, modeloDispositivo, marcaDispositivo, getEspacioPublico(codigoEspacioPublico));
		dispositivoDAO.update(dispositivoModificado);
	}
	
	@Override
	public void eliminarDispositivo(Integer codigoDispositivo) {
		dispositivoDAO.remove(intALong(codigoDispositivo));
	}
	

	@Override
	public List<DispositivoDTO> listarDispositivosDeUnEspacio(Integer codigoEspacioPublico) {
		
		List<Dispositivo> lista = dispositivoDAO.findAll();
		List<DispositivoDTO> dispositivosAsoc = new ArrayList<>();
		for (Dispositivo d : lista) {
			if (d.getEspacio().equals(getEspacioPublico(codigoEspacioPublico)))
				dispositivosAsoc.add(dispositivoADTO(d));
		}
		
		return dispositivosAsoc;
		
	}
	
	@Override
	public List<DispositivoDTO> listarDispositivos(Map<String,Filtro> filtros) {
		comprobarFiltrosEntidad(paraFiltrarDispositivo, paraProbarFiltrarDispositivo, filtros);
		return dispositivoDAO.findAll().stream()
				.filter(aplicarFiltrosParaEntidad(paraFiltrarDispositivo, filtros))
				.map(this::dispositivoADTO)
				.collect(Collectors.toList());
	}
	
	@Override
	public List<EspacioPublicoDTO> listarEspacioAsociadoDispositivos(Integer codigoEspacioPublico) {
		List<Dispositivo> lista = dispositivoDAO.findAll();
		List<EspacioPublicoDTO> espaciosAsoc = new ArrayList<>();
		for (Dispositivo d : lista) {
			if (d.getEspacio().equals(getEspacioPublico(codigoEspacioPublico)))
						espaciosAsoc.add(espacioADTO(d.getEspacio()));
			}
			
		return espaciosAsoc;
	}
	
	@Override
	public DispositivoDTO obtenerDispositivoPorCodigo(Integer codigoDispositivo) {
		return dispositivoADTO(getDispositivo(codigoDispositivo));
	}
	
	// Auxiliares
	private static Long intALong(Integer codigo) {
		if(codigo == null)
			return null;
		return codigo.longValue();
	}
	
	private Campana getCampana(Integer codigo) {
		Campana campana = campanaDAO.find(intALong(codigo));
		if(campana == null)
			throw new EntityNotFoundException("Campaña");
		return campana;
	}
	
	private EspacioPublico getEspacioPublico(Integer codigo) {
		EspacioPublico espacio = espacioPublicoDAO.find(intALong(codigo));
		if(espacio == null)
			throw new EntityNotFoundException("Espacio");
		return espacio;
	}
	
	private Tipo getTipo(Integer codigo) {
		Tipo tipo = tipoDAO.find(intALong(codigo));
		if(tipo == null)
			throw new EntityNotFoundException("Tipo");
		return tipo;
	}
	
	private Dispositivo getDispositivo(Integer codigo) {
		Dispositivo disp = dispositivoDAO.find(intALong(codigo));
		if(disp == null)
			throw new EntityNotFoundException("Dispositivo");
		return disp;
	}
	
	private CampanaDTO campanaADTO(Campana c) {
		return new CampanaDTO(c.getCodigo(),c.getFechaEdicion().toString(),c.getNombre(),c.getDescripcion());
	}
	
	private EspacioPublicoDTO espacioADTO(EspacioPublico ep) {
		return new EspacioPublicoDTO (ep.getCodigo(), ep.getNombre(), ep.getDireccion(), ep.getZonaDelimitada().getLatitud(), ep.getZonaDelimitada().getLongitud(), ep.getZonaDelimitada().getRadio(), ep.getTipo().getNombre(),
				ep.getDescripcion(), ep.getUrlImagen(), ep.getQr(), ep.getCampana().getNombre(), ep.getCampana().getCodigo(), ep.getTipo().getCodigo());
	}
	
	private TipoDTO tipoADTO(Tipo t) {
		return new TipoDTO(t.getCodigo(), t.getNombre());
	}
	
	private DispositivoDTO dispositivoADTO(Dispositivo d) {
		return new DispositivoDTO(d.getCodigo(), d.getNombre(), d.estaActivo(), d.getDescripcion(), d.getModelo(), d.getMarca(), d.getEspacio().getNombre(), d.getEspacio().getCodigo());
	}
	
	// Para filtros
	private Map<String,Function<Campana,Object>> paraFiltrarCampana = Map.of(
			"nombre", (campana) -> campana.getNombre(),
			"descripcion", (campana) -> campana.getDescripcion(),
			"codigo", (campana) -> campana.getCodigo(),
			"fechaEdicion", (campana) -> campana.getFechaEdicion()
		);
	
	private Map<String,Function<EspacioPublico,Object>> paraFiltrarEspacio = Map.of(
			"nombre", (espacio) -> espacio.getNombre(),
			"direccion", (espacio) -> espacio.getDireccion(),
			"descripcion", (espacio) -> espacio.getDescripcion(),
			"codigo", (espacio) -> espacio.getCodigo(),
			"codigoTipo", (espacio) -> espacio.getTipo().getCodigo(),
			"codigoCampaña", (espacio) -> espacio.getCampana().getCodigo()
		);

	private Map<String,Function<Dispositivo,Object>> paraFiltrarDispositivo = Map.of(
			"nombre", (dispositivo) -> dispositivo.getNombre(),
			"estado", (dispositivo) -> dispositivo.estaActivo(),
			"descripcion", (dispositivo) -> dispositivo.getDescripcion(),
			"modelo", (dispositivo) -> dispositivo.getModelo(),
			"marca", (dispositivo) -> dispositivo.getMarca(),
			"codigoEspacio", (dispositivo) -> dispositivo.getEspacio().getCodigo()
		);
	
	private Map<String,Object> paraProbarFiltrarCampana = Map.of(
			"nombre", "",
			"descripcion", "",
			"codigo", 0,
			"fechaEdicion", LocalDate.now() );
	
	private Map<String,Object> paraProbarFiltrarEspacio = Map.of(
			"nombre", "",
			"direccion", "",
			"descripcion", "",
			"codigo", 0,
			"codigoTipo", 0,
			"codigoCampaña", 0);
	
	private Map<String,Object> paraProbarFiltrarDispositivo = Map.of(
			"nombre", "",
			"estado", "",
			"descripcion", "",
			"modelo", 0,
			"marca", 0,
			"codigoEspacio", 0);
	
	
	private <E> void comprobarFiltrosEntidad(Map<String,Function<E,Object>> campos, Map<String,Object> valoresDePrueba, Map<String,Filtro> filtros) {
		if(filtros == null) return;
		for(String campo: filtros.keySet()) {
			if(!campos.containsKey(campo)) {
				throw new RuntimeException("No existe filtro para el campo "+campo);
			}
			// Probar filtro. Lanzará RuntimeException si falla.
			try {
				filtros.get(campo).evaluar( valoresDePrueba.get(campo) );
			} catch (RuntimeException e) {
				throw new RuntimeException(String.format("Error al aplicar filtro en campo '%s': %s", campo, e.getMessage()));
			}
		}
	}
	
	private <T> Predicate<T> aplicarFiltrosParaEntidad(Map<String,Function<T,Object>> campos, Map<String,Filtro> filtros) {
		Objects.requireNonNull(campos);
		if(filtros == null) {
			// Sin filtros, todos pasan.
			return (entidad) -> true;
		}
		return (entidad) -> {
			for(String campo: filtros.keySet()) {
				Function<T,Object> accion = campos.get(campo);
				return filtros.get(campo).evaluar(accion.apply(entidad));
			}
			return true;
		};
	}
	
	// Para ordenado

	private Map<String,Function<Campana,Comparable<?>>> paraOrdenarCampana = Map.of(
			"nombre", (campana) -> campana.getNombre(),
			"descripcion", (campana) -> campana.getDescripcion(),
			"codigo", (campana) -> campana.getCodigo(),
			"fechaEdicion", (campana) -> campana.getFechaEdicion()
		);

	private Map<String,Function<EspacioPublico,Comparable<?>>> paraOrdenarEspacio = Map.of(
			"nombre", (espacio) -> espacio.getNombre(),
			"direccion", (espacio) -> espacio.getDireccion(),
			"descripcion", (espacio) -> espacio.getDescripcion(),
			"codigo", (espacio) -> espacio.getCodigo(),
			"codigoTipo", (espacio) -> espacio.getTipo().getCodigo(),
			"codigoCampaña", (espacio) -> espacio.getCampana().getCodigo()
		);
	
	private <T, E extends Comparable<?>> Comparator<T> aplicarOrdenadoParaEntidad(Map<String,Function<T,E>> campos, Ordenado campo) {
		if(campo == null) {
			// No se indicó el campo. No ordenar.
			return (o1,o2) -> 0;
		}
		return (T o1, T o2) -> {
			var a = campos.get(campo.getCampo()).apply(o1);
			var b = campos.get(campo.getCampo()).apply(o2);
			return ((Comparable<E>)a).compareTo((E)b) * (campo.invertido() ? -1 : 1);
		};
	}
	
	private <E> void comprobarOrdenadoEntidad(Map<String,Function<E,Comparable<?>>> campos, Ordenado campoAOrdenar) {
		if(campoAOrdenar == null) return;
		if(!campos.containsKey(campoAOrdenar.getCampo())) {
			throw new RuntimeException("No existe el ordenado para el campo "+campoAOrdenar.getCampo());
		}
	}
	
}
