package ar.edu.unrn.seminario.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.management.modelmbean.ModelMBeanOperationInfo;

import ar.edu.unrn.seminario.dto.CampanaDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.EspacioPublicoDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.TipoDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.exception.EntityNotFoundException;
import ar.edu.unrn.seminario.modelo.Campana;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.EspacioPublico;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.Tipo;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.ZonaDelimitada;

public class MemoryApi implements IApi {

	private Map<Integer, Rol> roles = new HashMap<>();
	private Set<Usuario> usuarios = new HashSet<Usuario>();
	private Set<Campana> campanas = new HashSet<Campana>();
	private List<EspacioPublico> espaciospublicos = new ArrayList<>(); 
	private Set<Tipo> tipos = new HashSet<Tipo>();
	//map de espacios puede tener otra implementación
	private Set<Dispositivo> dispositivos = new HashSet<>(); 
	//lista dispositivos

	public MemoryApi() {

		// datos iniciales
		this.roles.put(1, new Rol(1, "ADMINISTRADOR"));
		this.roles.put(2, new Rol(2, "COMUNIDAD"));
		this.roles.put(3, new Rol(3, "GOBIERNO"));
		inicializarUsuarios();
		inicializarCampanas();
		inicializarTipos();
		inicializarEspaciosPublicos();
		inicializarDispositivos();
	}

	private void inicializarUsuarios() {
		registrarUsuario("mcambarieri", "1234", "mcambarieri@unrn.edu.ar", "Mauro", 3);
		registrarUsuario("ldifabio", "1234", "ldifabio@unrn.edu.ar", "Lucas", 2);
		registrarUsuario("admin", "1234", "admin@unrn.edu.ar", "Admin", 1);

	}
	
	private void inicializarCampanas() {
		LocalDate lt = LocalDate.now(); 
		
		registrarCampana(1, lt, "Verano", "Temporada de Verano");
		
		registrarCampana(2, lt, "Otoño", "Temporada de Otoño");
		
	}
	
	//INICIALIZAR DISPOSITIVOS Y METER EN LA LISTA CREADA
	private void inicializarDispositivos() {
		
		////////inicializacion de 2 listas de dispositivos   ////////
		registrarDispositivo(1, "Termómetro", true, "Mide temperatura", "QCY", "ATOM", 1);
		
		registrarDispositivo(2, "Barómetro", true, "Mide presión/altura", "SS", "OKEY", 2);
		
		
	}
	
	private void inicializarTipos() {
		
		////////inicializacion de 2 listas de dispositivos   ////////
		registrarTipo(1, "Plaza");
		registrarTipo(2, "Costanera");
		registrarTipo(3, "Parque");
		
	}
	
	private void inicializarEspaciosPublicos() {
		
//		registrarEspacioPublico(1, "Plaza San Martín", "San Martín 129", new ZonaDelimitada(1, 32, 12), "PLAZA", "Paseo, Picnic", "Plaza ubicada en el centro de Viedma", null, null, 1, null);
//		registrarEspacioPublico(2, "Costanera sur", "Av. Francisco de Viedma 540", new ZonaDelimitada(1, 32, 12), "COSTANERA", "Paseo, Picnic, Running, Ejercicio, Natación", "Costanera ubicada frente al Río Negro", null, null, 2, null);
		//Registrar tipos:
		
		
		
		//////// registra 2 espacios publicos ////////
		registrarEspacioPublico(1, "Plaza San Martín", "San Martín 129", 1, 32, 12, 1, "Plaza ubicada en el centro de Viedma", "imagen.jpg", "QR", 1);
		registrarEspacioPublico(2, "Costanera sur", "Av. Francisco de Viedma 540", 2, 22, 12, 2, "Costanera ubicada frente al Río Negro", "imagen12.jpg", "QR", 2);
		registrarEspacioPublico(3,"Parque Jorge Ferreira","Av.Pres. Raul Ricardo Alfonsin", 2,25,01, 3, "Espacio verde cerca de la costa del rio negro","imag.jpg", "QR", 1);
		asignarCampana(3, 1);
		
	}

	@Override
	public void registrarUsuario(String usuario, String contrasena, String email, String nombre, Integer codigoRol) {

		Rol rol = this.roles.get(codigoRol);
		Usuario usuarioNuevo = new Usuario(usuario, contrasena, nombre, email, rol);
		this.usuarios.add(usuarioNuevo);

	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		List<UsuarioDTO> dtos = new ArrayList<>();
		for (Usuario u : this.usuarios) {
			dtos.add(new UsuarioDTO(u.getUsuario(), u.getContrasena(), u.getNombre(), u.getEmail(),
					u.getRol().getNombre(), u.isActivo(), u.obtenerEstado()));
		}
		return dtos;
	}

	@Override
	public UsuarioDTO obtenerUsuario(String usuario) {
		for (Usuario u : usuarios) {
            if (u.getUsuario().equals(usuario)) {
            	return new UsuarioDTO(u.getUsuario(), u.getContrasena(), u.getNombre(), 
            			u.getEmail(), u.getRol().getNombre(), u.isActivo(), u.obtenerEstado());
            }
        }
		return null;
	}

	@Override
	public void eliminarUsuario(String usuario) {
		Usuario usuarioAEliminar = null;
        for (Usuario u : usuarios) {
            if (u.getUsuario().equals(usuario)) {
                usuarioAEliminar = u;
                break;
            }
        }
        if (usuarioAEliminar != null) {
            usuarios.remove(usuarioAEliminar);
        }
	}

	@Override
	public List<RolDTO> obtenerRoles() {
		List<RolDTO> dtos = new ArrayList<>();
		for (Rol r : this.roles.values()) {
			dtos.add(new RolDTO(r.getCodigo(), r.getNombre()));
		}
		return dtos;
	}

	@Override
	public List<RolDTO> obtenerRolesActivos() {
		List<RolDTO> dtos = new ArrayList<>();
		for (Rol r : this.roles.values()) {
			if (r.isActivo())
				dtos.add(new RolDTO(r.getCodigo(), r.getNombre()));
		}
		return dtos;
	}

	@Override
	public void guardarRol(Integer codigoRol, String descripcion, boolean estado) {
		Rol rol = new Rol(codigoRol, descripcion);
		this.roles.put(codigoRol, rol);
	}

	@Override
	public RolDTO obtenerRolPorCodigo(Integer codigoRol) {
        if (this.roles.containsKey(codigoRol)) {
        	Rol rol = roles.get(codigoRol);
        	return new RolDTO(rol.getCodigo(), rol.getNombre());
        }
        
		return null;
	}

	@Override
	public void activarRol(Integer codigoRol) {
		this.roles.get(codigoRol).activar();

	}

	@Override
	public void desactivarRol(Integer codigoRol) {
		this.roles.get(codigoRol).desactivar();
		
	}

	@Override
	public void activarUsuario(String usuario) {
		for (Usuario u : usuarios) {
			if (u.getUsuario().equals(usuario))
				u.activar();
			// enviar mail
			// ..
		}

	}

	@Override
	public void desactivarUsuario(String usuario) {
		for (Usuario u : usuarios) {
			if (u.getUsuario().equals(usuario))
				u.desactivar();

		}
	}
	
	
	// DECLARACIONES DE NUESTRAS FUNCIONES REFERIDAS A LAS CLASES CREADAS
	//
	//
	//XD

	@Override
	public void registrarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion) {
		//codigoCampana es campanas.size+1, +1 porque se va agregar uno 
		Campana campanaNueva = new Campana(this.campanas.size()+1, fecha, nombre, descripcion);
		this.campanas.add(campanaNueva);
	}

	@Override
	public void modificarCampana(Integer codigoCampana, LocalDate fecha, String nombre, String descripcion) {
		//get campaña
		Campana camp = getCampana(codigoCampana);
				camp.setDescripcion(descripcion);
				camp.setFechaEdicion(fecha);
				camp.setNombre(nombre);
		
	}

	@Override
	public void eliminarCampana(Integer codigoCampana) {
		campanas.remove( getCampana(codigoCampana) );
	}

	@Override
	public CampanaDTO obtenerCampanaPorCodigo(Integer codigoCampana) {
		
		//getCampana
		Campana c = getCampana(codigoCampana);
		return new CampanaDTO(c.getCodigo(),c.getFechaEdicion().toString(),c.getNombre(),c.getDescripcion());
	
	}
	
	@Override
	public List<CampanaDTO> listarCampanas(Map<String,Filtro> filtros, Ordenado campoAOrdenar) {
		
		List<CampanaDTO> dtos = new ArrayList<>();
		for (Campana c : this.campanas) {//Integer codigo, String fechaEdicion, String nombre, String descripcion
			dtos.add(new CampanaDTO(c.getCodigo(), c.getFechaEdicion().toString(), c.getNombre(), c.getDescripcion()));
		}
		return dtos;
	}
	
	@Override  //Ver la campaña a qué espacios está asociada.
	public List<EspacioPublicoDTO> listarEspaciosAsociados(Integer codigoCampana) {
		
		List<EspacioPublicoDTO> espacios = new ArrayList<>();
		for (EspacioPublico ep : espaciospublicos ) {
		    if(ep.getCampana().equals(getCampana(codigoCampana))) { //Lleva el objeto, creo que está mal que tome latitud + longitud + radio
		    		espacios.add(new EspacioPublicoDTO (ep.getCodigo(), ep.getNombre(), ep.getDireccion(), ep.getZonaDelimitada().getLatitud(), ep.getZonaDelimitada().getLongitud(), ep.getZonaDelimitada().getRadio(), ep.getTipo().getNombre(),
		    				ep.getDescripcion(), ep.getUrlImagen(), ep.getQr(), ep.getCampana().getNombre()));
		    }
		}
		
		return espacios;
	}

	
	
	//ESPACIOS
	
	@Override
	public void registrarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion,  double longitud, double latitud, double radio, Integer codigoTipo,
			String descripcion, String urlImagen, String qr, Integer codigoCampana) {
			
		ZonaDelimitada zona = new ZonaDelimitada(latitud, longitud, radio);
		codigoEspacioPublico = this.espaciospublicos.size()+1;
		EspacioPublico espacioNuevo = new EspacioPublico(codigoEspacioPublico, nombre, direccion, zona, getTipo(codigoTipo),
				descripcion, urlImagen, qr, getCampana(codigoCampana));
		this.espaciospublicos.add(espacioNuevo);
		
	}

	@Override
	public void modificarEspacioPublico(Integer codigoEspacioPublico, String nombre, String direccion, double longitud, double latitud, double radio, Integer codigoTipo,
			String descripcion, String urlImagen, String qr, Integer codigoCampana) {
		
		EspacioPublico espacioPublico = getEspacioPublico(codigoEspacioPublico);
		ZonaDelimitada zona = new ZonaDelimitada(latitud, longitud, radio);
		Campana campana = getCampanaOpcional(codigoCampana);
		Tipo tipo = getTipo(codigoTipo);
		
		espacioPublico.setNombre(nombre);
		espacioPublico.setDireccion(direccion);
		espacioPublico.setZonaDelimitada(zona);
		espacioPublico.setTipo(tipo);
		espacioPublico.setDescripcion(descripcion);
		espacioPublico.setUrlImagen(urlImagen);
		espacioPublico.setQr(qr);
		espacioPublico.setCampana(campana);
			
	}

	@Override
	public void eliminarEspacioPublico(Integer codigoEspacioPublico) {
		espaciospublicos.remove( getEspacioPublico(codigoEspacioPublico) );
	}

	@Override
	public EspacioPublicoDTO obtenerEspacioPublicoPorCodigo(Integer codigoEspacioPublico) {
		
		EspacioPublico ep = getEspacioPublico(codigoEspacioPublico);
		return new EspacioPublicoDTO(ep.getCodigo(), ep.getNombre(), ep.getDireccion(), ep.getZonaDelimitada().getLatitud(), ep.getZonaDelimitada().getLongitud(), ep.getZonaDelimitada().getRadio(), ep.getTipo().getNombre(), ep.getDescripcion(), ep.getUrlImagen(), ep.getQr(), ep.getCampana().getNombre(), ep.getCampana().getCodigo(), ep.getTipo().getCodigo());
		
	}
	
	@Override
	public List<EspacioPublicoDTO> listarEspaciosPublicos(Map<String,Filtro> filtros, Ordenado campoAOrdenar) {

		List<EspacioPublicoDTO> espacios = this.espaciospublicos.stream() // Stream<EspacioPublico>
			// Generar un DTO de cada EspacioPublico
			.map( (ep) -> new EspacioPublicoDTO(ep.getCodigo(), ep.getNombre(), ep.getDireccion(), ep.getZonaDelimitada().getLatitud(), ep.getZonaDelimitada().getLongitud(), ep.getZonaDelimitada().getRadio(), ep.getTipo().getNombre(), ep.getDescripcion(), ep.getUrlImagen(), ep.getQr(), ep.getCampana().getNombre()) ) // Stream<EspacioPublicoDTO>
					
			// Ordenar por nombre
			.sorted( (ep1, ep2) -> {	
				return ep1.getNombre().compareToIgnoreCase(ep2.getNombre());
			} )	// Stream<EspacioPublicoDTO>
			// Pasar a List<EspacioPublicoDTO>
			.collect(Collectors.toList());
		return espacios;  
	}
	
		
	private void asignarCampana(Integer codigoEspacioPublico, Integer codigoCampana) {
	
			
			EspacioPublico espacioPublico = getEspacioPublico(codigoEspacioPublico);
			espacioPublico.setCampana(getCampana(codigoCampana));	
		
	}
	
	
	//TIPO
	@Override
	public void registrarTipo(Integer codigoTipo, String nombreTipo) {
			
			codigoTipo = this.tipos.size()+1;
			Tipo tipoNuevo = new Tipo(codigoTipo, nombreTipo);
			tipos.add(tipoNuevo);
		
	}
	

	@Override
	public void modificarTipo(Integer codigoTipo, String nombreTipo) {
		
			Tipo tipoModificar = getTipo(codigoTipo);
			tipoModificar.setNombre(nombreTipo);
			
	}
	
	@Override
	public void eliminarTipo(Integer codigoTipo) {
		
			tipos.remove( getTipo(codigoTipo) );
		
	}
	

	@Override
	public TipoDTO obtenerTipoPorCodigo(Integer codigoTipo) {
		
			Tipo tipo = getTipo(codigoTipo);
			return new TipoDTO(tipo.getCodigo(), tipo.getNombre());
		
	}

	@Override
	public List<TipoDTO> listarTipos() {

		List<TipoDTO> dtos = new ArrayList<>();
		for (Tipo t : this.tipos) {//Integer codigo, String fechaEdicion, String nombre, String descripcion
			dtos.add(new TipoDTO(t.getCodigo(), t.getNombre()));
		}
		return dtos;
		
	
	}
	
	private void asignarTipo(Integer codigoEspacioPublico, Integer codigoTipo) {
		
		EspacioPublico espacioPublico = getEspacioPublico(codigoEspacioPublico);
		espacioPublico.setTipo(getTipo(codigoTipo));	
		
	}


	//ESTE METODO POR EL MOMENTO MANEJA DISPOSITIVO, DEBERÍA MANEJAR LISTA DISPOSITIVODTO
	@Override 
	public List<DispositivoDTO> listarDispositivosDeUnEspacio(Integer codigoEspacioPublico) {
		
		List<DispositivoDTO> dispositivosAsoc = new ArrayList<>();
		for (Dispositivo d : dispositivos) {
			if (d.getEspacio().equals(getEspacioPublico(codigoEspacioPublico)))
				dispositivosAsoc.add(new DispositivoDTO(d.getCodigo(), d.getNombre(), d.estaActivo(), d.getDescripcion(), d.getModelo(), d.getMarca(), d.getEspacio().getNombre(), d.getEspacio().getCodigo()));
		}
		return dispositivosAsoc;  
	}
	
	
	@Override
	public List<DispositivoDTO> listarDispositivos(Map<String,Filtro> filtros) {
		List<DispositivoDTO> dtos = new ArrayList<>();
		for (Dispositivo d : this.dispositivos) {//Integer codigo, String fechaEdicion, String nombre, String descripcion
			dtos.add(new DispositivoDTO(d.getCodigo(), d.getNombre(), d.estaActivo(), d.getDescripcion(), d.getModelo(), d.getMarca(), d.getEspacio().getNombre(), d.getEspacio().getCodigo()));
		}
		return dtos;
	}

	//Este es para inicializar dispositivos de PRUEBA
	@Override
	public void registrarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo, String descripcionDispositivo, 
			String modeloDispositivo, String marcaDispositivo, Integer espacioPublico) {
		
		Dispositivo dispNuevo = new Dispositivo(codigoDispositivo, nombreDispositivo, estadoDispositivo, descripcionDispositivo,
													modeloDispositivo, marcaDispositivo, getEspacioPublico(espacioPublico));
		dispositivos.add(dispNuevo);
	}
	
	
	@Override
	public void modificarDispositivo(Integer codigoDispositivo, String nombreDispositivo, Boolean estadoDispositivo, 
			String descripcionDispositivo, String modeloDispositivo, String marcaDispositivo, Integer espacioPublico) {
		Dispositivo disp = getDispositivo(codigoDispositivo);
		disp.setNombre(nombreDispositivo);
		if(estadoDispositivo) // == true
			disp.activar();
		else
			disp.desactivar();
		disp.setDescripcion(descripcionDispositivo);
		disp.setModelo(modeloDispositivo);
		disp.setMarca(marcaDispositivo);
		disp.setEspacio(getEspacioPublico(espacioPublico));
	}
	
	@Override
	public void eliminarDispositivo(Integer codigoDispositivo) {
		dispositivos.remove(getDispositivo(codigoDispositivo));
	}
	
	
	@Override
	public DispositivoDTO obtenerDispositivoPorCodigo(Integer codigoDispositivo) {
		
			Dispositivo d = getDispositivo(codigoDispositivo);
			return new DispositivoDTO(d.getCodigo(), d.getNombre(), d.estaActivo(), d.getDescripcion(), d.getModelo(), d.getMarca(), d.getEspacio().getNombre(), d.getEspacio().getCodigo());
		
	}
	
	@Override
	public List<EspacioPublicoDTO> listarEspacioAsociadoDispositivos(Integer codigoEspacioPublico) {
		List<EspacioPublicoDTO> espaciosAsoc = new ArrayList<>();
		for (Dispositivo d : this.dispositivos) {
			if (d.getEspacio().equals(getEspacioPublico(codigoEspacioPublico)))
						espaciosAsoc.add(new EspacioPublicoDTO(d.getEspacio().getCodigo(), d.getEspacio().getNombre(), d.getEspacio().getDireccion(), d.getEspacio().getZonaDelimitada().getLatitud(), d.getEspacio().getZonaDelimitada().getLongitud(), d.getEspacio().getZonaDelimitada().getRadio(),
								d.getEspacio().getTipo().getNombre(), d.getEspacio().getDescripcion(), d.getEspacio().getUrlImagen(), d.getEspacio().getQr(), d.getEspacio().getCampana().getNombre()));
			}
			
		return espaciosAsoc;
		
	}
	
	@Override
	public void exportarCampanas(String... ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exportarEspaciosPublicos(String... ids) {
		// TODO Auto-generated method stub
		
	}
	
	// Auxiliares (EntityNotFoundException)
	
	private Campana getCampana(Integer codigo) {
		return campanas.stream()
				.filter( (campana) -> campana.getCodigo().equals(codigo) )
				.findFirst().orElseThrow( () -> new EntityNotFoundException("Campaña") );
	}
	
	private EspacioPublico getEspacioPublico(Integer codigo) {
		return espaciospublicos.stream()
				.filter( (espacios) -> espacios.getCodigo().equals(codigo) )
				.findFirst().orElseThrow( () -> new EntityNotFoundException("Espacio") );
	}
	
	private Tipo getTipo(Integer codigo) {
		return tipos.stream()
				.filter( (tipo) -> tipo.getCodigo().equals(codigo) )
				.findFirst().orElseThrow( () -> new EntityNotFoundException("Tipo") );
	}
	
	private Dispositivo getDispositivo(Integer codigo) {
		return dispositivos.stream()
				.filter( (dispositivo) -> dispositivo.getCodigo().equals(codigo) )
				.findFirst().orElseThrow( () -> new EntityNotFoundException("Dispositivo") );
	}
	
	
	private Campana getCampanaOpcional(Integer codigo) {
		// Si se especificó una campaña, tirar error (EntityNotFoundException) si no se encuentra.
		// Si codigoCampana es null, se toma como desasignar campana.
		// TODO: cambiar esta implementación ya que por ahora es solo para pruebas.
		return codigo==null ? null : getCampana(codigo);
	}

}
