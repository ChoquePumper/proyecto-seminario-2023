package ar.edu.unrn.seminario.api;

import java.util.Objects;

public class Ordenado {
	private String campo;
	private boolean invertido;
	public Ordenado(String campo, boolean invertido) {
		this.campo = Objects.requireNonNull(campo);
		this.invertido = invertido;
	}
	public String getCampo() {
		return campo;
	}
	public boolean invertido() {
		return invertido;
	}
}
