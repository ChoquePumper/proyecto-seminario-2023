package ar.edu.unrn.seminario.accesos;

import java.util.List;

public interface ObjetoDAO<E> {

	void create(E obj);
	void update(E obj);
	void remove(Long id);
	void remove(E obj);
	E find(Long id);
	List<E> findAll();
}
