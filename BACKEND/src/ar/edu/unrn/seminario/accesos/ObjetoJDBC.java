package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import ar.edu.unrn.seminario.commons.Validaciones;

public abstract class ObjetoJDBC {
	private final String subprotocolo;
	private final String subnombre;
	
	private final String lineaUrlJDBC;
	// Ejemplo de UrlJDBC: "jdbc:mysql://servidor:3306/schema"
	
	/** Arma la linea JDBC que luego se usará al invocar {@code getConnection}
	 * @param subprotocolo : Nombre de subprotocolo.
	 * Ej: {@code "odbc"}, {@code "mysql"}, {@code "db2"}, {@code "oracle"}, {@code "postgresql"}.
	 * @param subnombre : Nombre de servidor y/o base de datos. Dependiendo del subprotocolo, puede ser:
	 * <ul>
	 *   <li>"database"</li>
	 *   <li>"//host/database"</li>
	 *   <li>"//host:port/database"</li>
	 * </ul>
	 */
	public ObjetoJDBC(String subprotocolo, String subnombre) {
		this.subprotocolo = (Validaciones.validarDato(subprotocolo, "subprotocolo"));
		this.subnombre = Validaciones.validarDato(subnombre, "servidor");
		this.lineaUrlJDBC = String.format("jdbc:%s:%s", this.subprotocolo, this.subnombre);
	}
	
	private static String validarSubprotocolo(String subprotocolo) {
		subprotocolo = subprotocolo.trim();
		if (!esNombreValido(subprotocolo))
			throw new IllegalArgumentException("Formato de subprotocolo invalido");
		return subprotocolo;
	}
	
	private static boolean esNombreValido(String nombre) {
		return Pattern.matches("[a-zA-Z0-9]", nombre);
	}

	/** Llamar a {@code DriverManager.getConnection()} */
	protected Connection getConnection() throws SQLException {
		//return DriverManager.getConnection(lineaUrlJDBC);
		return getConnection("root", "");
	}
	
	protected Connection getConnection(String user, String password) throws SQLException {
		return DriverManager.getConnection(lineaUrlJDBC, user, password);
	}
	
	/** Invoca a los métodos set correspondientes de {@code PreparedStatement} según los tipos */
	protected static PreparedStatement setearDatos(PreparedStatement statement, Object...objects) throws SQLException {
		int indice = 1;
		// Mapa de algunos tipos de datos para PreparedStatement. Podría agregar más.
		Map<Class<?>, StatementSetData> tipos = Map.of(
				String.class,    (i,o) -> statement.setString(i, (String)o),
				Boolean.class,   (i,o) -> statement.setBoolean(i, (Boolean)o),
				Integer.class,   (i,o) -> statement.setObject(i, (Integer)o, Types.INTEGER),
				Long.class,      (i,o) -> statement.setObject(i, (Long)o, Types.INTEGER),
				Float.class,     (i,o) -> statement.setObject(i, (Float)o, Types.FLOAT),
				Double.class,    (i,o) -> statement.setObject(i, (Double)o, Types.DOUBLE),
				Date.class,      (i,o) -> statement.setDate(i, (Date)o)
		);
		for(Object o : objects) {
			if (o instanceof Nulo)
				// Al pasar (Tipo)null en la lista queda en (Object)null y por eso
				// hice una clase que indique que es null pero con un tipo.
				// Ahora que lo pienso, podría haber usado Optional sin crear otra clase.
				tipos.get( ((Nulo<?>)o).getClase() ).setData(indice, null);
			else
				tipos.get(o.getClass()).setData(indice, o);
			indice++;
		}
		return statement;
	}
	
	protected static Integer longAInteger(Long l) {
		if(l == null)
			return null;
		return l.intValue();
	}
	
	protected static Object posibleNulo(Integer obj) {
		if(obj != null)
			return obj;
		return Nulo.of(Integer.class);
	}
	
	protected boolean esDuplicado(SQLIntegrityConstraintViolationException e) {
		return (e.getSQLState().startsWith(MARIADB_CLASSCODE_23)) && 
				(e.getErrorCode() == MARIADB_ER_DUP_ENTRY);
	}
	
	// Algunos códigos:
	static final String MARIADB_CLASSCODE_23 = "23";
	static final int MARIADB_ER_DUP_ENTRY = 1062;
}

@FunctionalInterface
interface StatementSetData {
	void setData(int i, Object o) throws SQLException;
}

/**
 * Esta clase la cree para indicar valores nulos pero con Tipo. 
 * @param <T> Tipo de dato.
 */
class Nulo<T> {
	private Class<T> clase;
	public Nulo(Class<T> clase) {
		this.clase = Objects.requireNonNull(clase);
	}
	public Class<T> getClase() {
		return clase;
	}
	public T get() {
		return null;
	}
	
	/**
	 * Alias para: {@code new Nulo<T>(T.class)}
	 * @param clase El tipo de dato a indicar. Ej: {@code Integer.class}
	 */
	static <T> Nulo<T> of(Class<T> clase) {
		return new Nulo<T>(clase);
	}
	
}
