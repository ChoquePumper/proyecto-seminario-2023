package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTransientConnectionException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.modelo.Campana;

public class CampanaDAOJDBC extends ObjetoJDBC implements ObjetoDAO<Campana> {
	
	private static final String mensajeErrDuplicado = "Ya existe otra campaña con el mismo nombre.";
	
	/** @see ObjetoJDBC#ObjetoJDBC(String,String) */
	public CampanaDAOJDBC(String subprotocolo, String subnombre) {
		super(subprotocolo, subnombre);
	}

	@Override
	public void create(Campana obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"INSERT INTO campañas (fecha_edicion, nombre, descripcion) values (?,?,?);");)
		{
			sent.setDate(1, Date.valueOf(obj.getFechaEdicion()) );
			sent.setString(2, obj.getNombre() );
			sent.setString(3, obj.getDescripcion() );
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException("Error al crear la campaña.", e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("Error al crear la campaña.", e);
		}
	}

	@Override
	public void update(Campana obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"UPDATE campañas SET fecha_edicion = ?, nombre = ?, descripcion = ? WHERE codigo = ?;");)
		{	
			sent.setDate(1, Date.valueOf(obj.getFechaEdicion()) );
			sent.setString(2, obj.getNombre() );
			sent.setString(3, obj.getDescripcion() );
			sent.setInt(4, obj.getCodigo());
			int filas = sent.executeUpdate();
			if(filas == 0)
				throw new AppException(String.format("La campaña con código %d no existe.",obj.getCodigo()));
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException(String.format("Error al actualizar la campaña con código %d.",obj.getCodigo()), e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al actualizar la campaña con código %d.",obj.getCodigo()), e);
		}
	}

	@Override
	public void remove(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("DELETE FROM campañas WHERE codigo = ? ;");)
		{	
			sent.setInt(1, id.intValue());
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Posiblemente algún espacio público hace referencia a esta campaña. 
			throw new IntegrityException(e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al eliminar la campaña con código %d.",id.intValue()), e);
		}
	}

	@Override
	public void remove(Campana obj) {
		remove(obj.getCodigo().longValue());
	}

	@Override
	public Campana find(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("SELECT * FROM campañas WHERE codigo = ? ;");
			ResultSet res = setearDatos(sent, posibleNulo(longAInteger(id))).executeQuery(); )
		{
			if(res.next())
				return deResultSet(res);
		}
		catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		}
		catch (SQLException e) {
			throw new AppException(String.format("No se pudo obtener la campaña con código %d.",longAInteger(id)), e);
		}
		return null;
	}

	@Override
	public List<Campana> findAll() {
		List<Campana> lista = new ArrayList<>();
		try (Connection conn = getConnection();
				Statement sent = conn.createStatement();
				ResultSet res = sent.executeQuery("SELECT * FROM campañas;");)
		{
			while(res.next()) {
				lista.add( deResultSet(res) );
			}
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("No se pudo obtener la lista de campañas.", e);
		}
		return lista;
	}

	// Auxiliares
	private Campana deResultSet(ResultSet res) throws SQLException {
		return new Campana(
			res.getInt("codigo"),
			res.getDate("fecha_edicion").toLocalDate(),
			res.getString("nombre"),
			res.getString("descripcion")
		);
	}
}
