package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTransientConnectionException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.edu.unrn.seminario.modelo.Tipo;

public class TipoDAOJDBC extends ObjetoJDBC implements ObjetoDAO<Tipo> {

	private static final String mensajeErrDuplicado = "Ya existe otro tipo de espacio público con el mismo nombre.";
	
	/** @see ObjetoJDBC#ObjetoJDBC(String,String) */
	public TipoDAOJDBC(String subprotocolo, String subnombre) {
		super(subprotocolo, subnombre);
	}

	@Override
	public void create(Tipo obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"INSERT INTO tipos (nombre) values (?);");)
		{
			sent.setString(1, obj.getNombre() );
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException("Error al crear el tipo de espacio público.", e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("Error al crear el tipo de espacio público.", e);
		}
	}

	@Override
	public void update(Tipo obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"UPDATE tipos SET nombre = ? WHERE codigo = ?;");)
		{
			sent.setString(1, obj.getNombre() );
			sent.setInt(2, obj.getCodigo());
			int filas = sent.executeUpdate();
			if(filas == 0)
				throw new AppException(String.format("El tipo de espacio público con código %d no existe.",obj.getCodigo()));
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException(String.format("Error al actualizar el tipo de espacio público con código %d.",obj.getCodigo()), e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al actualizar el tipo de espacio público con código %d.",obj.getCodigo()), e);
		}
	}

	@Override
	public void remove(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("DELETE FROM tipos WHERE codigo = ? ;");)
		{	
			sent.setInt(1, id.intValue());
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Posiblemente algún espacio público hace referencia a este tipo de espacio público. 
			throw new IntegrityException(e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al eliminar el tipo de espacio público con código %d.",id.intValue()), e);
		}
	}

	@Override
	public void remove(Tipo obj) {
		remove(obj.getCodigo().longValue());
	}

	@Override
	public Tipo find(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("SELECT * FROM tipos WHERE codigo = ? ;");
			ResultSet res = setearDatos(sent, posibleNulo(longAInteger(id))).executeQuery(); )
		{
			if(res.next())
				return deResultSet(res);
		}
		catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		}
		catch (SQLException e) {
			throw new AppException(String.format("No se pudo obtener el tipo de espacio público con código %d.",longAInteger(id)), e);
		}
		return null;
}

	@Override
	public List<Tipo> findAll() {
		List<Tipo> lista = new ArrayList<>();
		try (Connection conn = getConnection();
				Statement sent = conn.createStatement();
				ResultSet res = sent.executeQuery("SELECT * FROM tipos;");)
		{
			while(res.next()) {
				lista.add( deResultSet(res) );
			}
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("No se pudo obtener la lista de tipos de espacios públicos.", e);
		}
		return lista;
	}
	
	// Auxiliares
	private Tipo deResultSet(ResultSet res) throws SQLException {
		return new Tipo(
			res.getInt("codigo"),
			res.getString("nombre")
		);
	}

}
