package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTransientConnectionException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.edu.unrn.seminario.modelo.Campana;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.EspacioPublico;
import ar.edu.unrn.seminario.modelo.Tipo;
import ar.edu.unrn.seminario.modelo.ZonaDelimitada;

public class DispositivoDAOJBDC extends ObjetoJDBC implements ObjetoDAO<Dispositivo> {

	private static final String mensajeErrDuplicado = "Ya existe otro dispositivo con el mismo nombre.";
	
	/** @see ObjetoJDBC#ObjetoJDBC(String,String) */
	public DispositivoDAOJBDC(String subprotocolo, String subnombre) {
		super(subprotocolo, subnombre);
	}

	@Override
	public void create(Dispositivo obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"INSERT INTO dispositivos (nombre, estado, descripcion, "
					+ "modelo, marca, codigo_espacio) values (?, ?, ?, ?, ?, ?);");)
		{
			sent.setString(1, obj.getNombre() );
			sent.setBoolean(2, obj.estaActivo() );
			sent.setString(3, obj.getDescripcion() );
			sent.setString(4, obj.getModelo() );
			sent.setString(5, obj.getMarca() );
			sent.setInt(6, obj.getEspacio().getCodigo() );
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException("Error al crear el dispositivo.", e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("Error al crear el dispositivo.", e);
		}
	}

	@Override
	public void update(Dispositivo obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"UPDATE dispositivos SET nombre=?, estado=?, descripcion=?, "
							+ "modelo=?, marca=?, codigo_espacio=? "
							+ "WHERE codigo = ?;");)
		{
			// SET:
			sent.setString(1, obj.getNombre() );
			sent.setBoolean(2, obj.estaActivo() );
			sent.setString(3, obj.getDescripcion() );
			sent.setString(4, obj.getModelo() );
			sent.setString(5, obj.getMarca() );
			sent.setInt(6, obj.getEspacio().getCodigo() );
			// WHERE:
			sent.setInt(7, obj.getCodigo());
			sent.executeUpdate();
			int filas = sent.executeUpdate();
			if(filas == 0)
				throw new AppException(String.format("El dispositivo con código %d no existe.",obj.getCodigo()));
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException(String.format("Error al actualizar el dispositivo con código %d.",obj.getCodigo()), e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al actualizar el dispositivo con código %d.",obj.getCodigo()), e);
		}
	}

	@Override
	public void remove(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("DELETE FROM dispositivos WHERE codigo = ? ;");)
		{	
			sent.setInt(1, id.intValue());
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Posiblemente algún espacio público hace referencia a esta campaña. 
			throw new IntegrityException(e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al eliminar el dispositivo con código %d.",id.intValue()), e);
		}
	}

	@Override
	public void remove(Dispositivo obj) {
		remove(obj.getCodigo().longValue());
	}

	@Override
	public Dispositivo find(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("SELECT * FROM dispositivos D JOIN espaciospublicos ES ON (D.codigo_espacio = ES.codigo) JOIN tipos T ON (ES.codigo_tipo = T.codigo) JOIN campañas C ON (ES.codigo_campaña = C.codigo) WHERE D.codigo = ?;");
			ResultSet res = setearDatos(sent, posibleNulo(longAInteger(id))).executeQuery(); )
		{
			if(res.next())
				return deResultSet(res);
		}
		catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		}
		catch (SQLException e) {
			throw new AppException(String.format("No se pudo obtener el dispositivo con código %d.",longAInteger(id)), e);
		}
		return null;
}

	@Override
	public List<Dispositivo> findAll() {
		List<Dispositivo> lista = new ArrayList<>();
		try (Connection conn = getConnection();
				Statement sent = conn.createStatement();
				ResultSet res = sent.executeQuery("SELECT * FROM dispositivos D JOIN espaciospublicos ES ON (D.codigo_espacio = ES.codigo) JOIN tipos T ON (ES.codigo_tipo = T.codigo) JOIN campañas C ON (ES.codigo_campaña = C.codigo);");)
		{
			while(res.next()) {
				lista.add( deResultSet(res) );
			}
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("No se pudo obtener la lista dispositivos.", e);
		}
		return lista;
	}
	
	// Auxiliares
	private Dispositivo deResultSet(ResultSet res) throws SQLException {
		ZonaDelimitada zona = new ZonaDelimitada(
				res.getDouble("ES.latitud"), res.getDouble("ES.longitud"), res.getDouble("ES.radio"));
		
		Campana camp = new Campana(
				res.getInt("C.codigo"),
				res.getDate("C.fecha_edicion").toLocalDate(),
				res.getString("C.nombre"),
				res.getString("C.descripcion")
		);
		
		Tipo tipo =  new Tipo(
				res.getInt("T.codigo"),
				res.getString("T.nombre")
			);
		
		EspacioPublico esp = new EspacioPublico(
				res.getInt("ES.codigo"), 
				res.getString("ES.nombre"), 
				res.getString("ES.direccion"),
				zona, 
				tipo, 
				res.getString("ES.descripcion"),
				res.getString("ES.url_imagen"), 
				res.getString("ES.url_qr"), 
				camp
			);
		
		return new Dispositivo(
			res.getInt("codigo"),
			res.getString("nombre"),
			res.getBoolean("estado"),
			res.getString("descripcion"),
			res.getString("modelo"),
			res.getString("marca"),
			esp
		);
	}
	
	
	
	
}
