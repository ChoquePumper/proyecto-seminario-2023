package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTransientConnectionException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.AppException;
import ar.edu.unrn.seminario.exception.DataAccessException;
import ar.edu.unrn.seminario.exception.DuplicateException;
import ar.edu.unrn.seminario.exception.IntegrityException;
import ar.edu.unrn.seminario.modelo.Campana;
import ar.edu.unrn.seminario.modelo.EspacioPublico;
import ar.edu.unrn.seminario.modelo.Tipo;
import ar.edu.unrn.seminario.modelo.ZonaDelimitada;

public class EspacioPublicoDAOJDBC extends ObjetoJDBC implements ObjetoDAO<EspacioPublico>{
	
	private static final String mensajeErrDuplicado = "Ya existe otra campaña con el mismo nombre.";

	/** @see ObjetoJDBC#ObjetoJDBC(String,String) */
	public EspacioPublicoDAOJDBC(String subprotocolo, String subnombre) {
		super(subprotocolo, subnombre);
	}

	@Override
	public void create(EspacioPublico obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"INSERT INTO espaciospublicos (nombre, direccion, latitud, "
					+ "longitud, radio, codigo_tipo, descripcion, url_imagen, "
					+ "url_qr, codigo_campaña) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");)
		{
			ZonaDelimitada zona = obj.getZonaDelimitada();
			setearDatos(sent,
					obj.getNombre(), obj.getDireccion(), zona.getLatitud(),
					zona.getLongitud(), zona.getRadio(), obj.getTipo().getCodigo(), obj.getDescripcion(), obj.getUrlImagen(),
					obj.getQr(), obj.getCampana().getCodigo()
			);
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException("Error al crear el espacio público.", e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("Error al crear el espacio público.", e);
		}
	}

	@Override
	public void update(EspacioPublico obj) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement(
					"UPDATE espaciospublicos SET nombre=?, direccion=?, latitud=?, "
					+ "longitud=?, radio=?, codigo_tipo=?, descripcion=?, url_imagen=?, "
					+ "url_qr=?, codigo_campaña=? WHERE codigo = ?;");)
		{	
			ZonaDelimitada zona = obj.getZonaDelimitada();
			setearDatos(sent,
					obj.getNombre(), obj.getDireccion(), zona.getLatitud(),
					zona.getLongitud(), zona.getRadio(), obj.getTipo().getCodigo(), obj.getDescripcion(), obj.getUrlImagen(),
					obj.getQr(), obj.getCampana().getCodigo(),  /*WHERE codigo =*/ obj.getCodigo()
			);
			int filas = sent.executeUpdate();
			if(filas == 0)
				throw new AppException(String.format("El espacio público con código %d no existe.",obj.getCodigo()));
		} catch (SQLIntegrityConstraintViolationException e) {
			if (esDuplicado(e)) {
				throw new DuplicateException(mensajeErrDuplicado);
			}
			throw new IntegrityException(String.format("Error al actualizar el espacio público con código %d.",obj.getCodigo()), e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al actualizar el espacio público con código %d.",obj.getCodigo()), e);
		}
	}

	@Override
	public void remove(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("DELETE FROM espaciospublicos WHERE codigo = ? ;");)
		{	
			sent.setInt(1, id.intValue());
			sent.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Posiblemente algún espacio público hace referencia a este espacio público.
			throw new IntegrityException(e);
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException(String.format("Error al eliminar el espacio público con código %d.",id.intValue()), e);
		}
	}

	@Override
	public void remove(EspacioPublico obj) {
		remove(obj.getCodigo().longValue());
	}

	@Override
	public EspacioPublico find(Long id) {
		try (Connection conn = getConnection();
			PreparedStatement sent = conn.prepareStatement("SELECT * FROM espaciospublicos ES JOIN tipos T ON (ES.codigo_tipo = T.codigo) JOIN campañas C ON (ES.codigo_campaña = C.codigo) WHERE ES.codigo = ? ;");
			ResultSet res = setearDatos(sent, posibleNulo(longAInteger(id))).executeQuery(); )
		{
			if(res.next())
				return deResultSet(res);
		}
		catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		}
		catch (SQLException e) {
			throw new AppException(String.format("No se pudo obtener el espacio público con código %d.",longAInteger(id)), e);
		}
		return null;
	}

	@Override
	public List<EspacioPublico> findAll() {
		List<EspacioPublico> lista = new ArrayList<>();
		try (Connection conn = getConnection();
				Statement sent = conn.createStatement();
				ResultSet res = sent.executeQuery("SELECT * FROM espaciospublicos ES JOIN tipos T ON (ES.codigo_tipo = T.codigo) JOIN campañas C ON (ES.codigo_campaña = C.codigo);");)
		{
			while(res.next()) {
				lista.add( deResultSet(res) );
			}
		} catch (SQLTransientConnectionException | SQLNonTransientConnectionException e) {
			// Error de conexión con la base de datos
			throw new DataAccessException("No se pudo conectar con la base de datos", e);
		} catch (SQLException e) {
			throw new AppException("No se pudo obtener la lista de espacios públicos.", e);
		}
		return lista;
	}
	
	// Auxiliares
	public EspacioPublico deResultSet(ResultSet res) throws SQLException {
		
		ZonaDelimitada zona = new ZonaDelimitada(
				res.getDouble("ES.latitud"), res.getDouble("ES.longitud"), res.getDouble("ES.radio"));
		
		Campana camp = new Campana(
				res.getInt("C.codigo"),
				res.getDate("C.fecha_edicion").toLocalDate(),
				res.getString("C.nombre"),
				res.getString("C.descripcion")
		);
		
		Tipo tipo =  new Tipo(
				res.getInt("T.codigo"),
				res.getString("T.nombre")
			);
		
		return new EspacioPublico(
				res.getInt("ES.codigo"), 
				res.getString("ES.nombre"), 
				res.getString("ES.direccion"),
				zona, 
				tipo, 
				res.getString("ES.descripcion"),
				res.getString("ES.url_imagen"), 
				res.getString("ES.url_qr"), 
				camp
			);
	}

}
