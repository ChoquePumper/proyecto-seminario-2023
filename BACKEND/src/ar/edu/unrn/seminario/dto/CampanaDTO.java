package ar.edu.unrn.seminario.dto;

public class CampanaDTO {
	private Integer codigo;
	private String fechaEdicion;
	private String nombre;
	private String descripcion;
	
	public CampanaDTO(Integer codigo, String fechaEdicion, String nombre, String descripcion) {
		super();
		this.codigo = codigo;
		this.fechaEdicion = fechaEdicion;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getFechaEdicion() {
		return fechaEdicion;
	}
	
	public void setFechaEdicion(String fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
