package ar.edu.unrn.seminario.dto;


public class DispositivoDTO {

	private Integer codigo;
	private String nombre;
	private Boolean estado;
	private String descripcion;
	private String modelo;
	private String marca;
	private String nombreEspacio;
	private Integer codigoEspacio;
	
	public DispositivoDTO(Integer codigo, String nombre, Boolean estado, String descripcion, String modelo, String marca, String nombreEspacio, Integer codigoEspacio) {
		this.codigo = codigo;
		this.estado = estado;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.modelo = modelo;
		this.marca = marca;
		this.nombreEspacio = nombreEspacio;
		this.codigoEspacio = codigoEspacio;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getNombreEspacio() {
		return nombreEspacio;
	}

	public void setNombreEspacio(String nombreEspacio) {
		this.nombreEspacio = nombreEspacio;
	}

	public Integer getCodigoEspacio() {
		return codigoEspacio;
	}

	public void setCodigoEspacio(Integer codigoEspacio) {
		this.codigoEspacio = codigoEspacio;
	}

}
