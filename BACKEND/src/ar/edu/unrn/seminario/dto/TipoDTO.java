package ar.edu.unrn.seminario.dto;

public class TipoDTO {
	
	private Integer codigo;
	private String nombre;
	
	public TipoDTO(Integer codigoTipo, String nombreTipo) {
		super();
		this.codigo = codigoTipo;
		this.nombre = nombreTipo;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

}
