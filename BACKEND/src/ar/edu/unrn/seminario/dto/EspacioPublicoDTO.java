package ar.edu.unrn.seminario.dto;

public class EspacioPublicoDTO {
	private Integer codigo;
	private String nombre;
	private String direccion;
	private Double latitud;
	private Double longitud;
	private Double radio;
	private String tipo;
	private String descripcion;
	private String urlImagen;
	private String urlQR;
	private String nombreCampana;
	// Otros códigos
	private Integer codigoCampana;
	private Integer codigoTipo;
	
	public EspacioPublicoDTO(Integer codigo, String nombre, String direccion, Double latitud, Double longitud, Double radio, String tipo,
			String descripcion, String urlImagen, String urlQR, String nombreCampana) {
		this.codigo=codigo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.latitud = latitud;
		this.longitud = longitud;
		this.radio = radio;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.urlImagen = urlImagen;
		this.urlQR = urlQR;
		this.nombreCampana=nombreCampana;
	}
	
	public EspacioPublicoDTO(Integer codigo, String nombre, String direccion, Double latitud, Double longitud, Double radio, String tipo,
			String descripcion, String urlImagen, String urlQR, String nombreCampana, Integer codigoCampana, Integer codigoTipo) {
		this(codigo, nombre, direccion, latitud, longitud, radio, tipo, descripcion, urlImagen, urlQR, nombreCampana);
		this.codigoCampana = codigoCampana;
		this.codigoTipo = codigoTipo;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo=codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	
	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	
	public Double getRadio() {
		return radio;
	}

	public void setRadio(Double radio) {
		this.radio = radio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public String getQr() {
		return urlQR;
	}

	public void setQr(String urlQR) {
		this.urlQR = urlQR;
	}
	
	public String getNombreCampana() {
		return nombreCampana;
	}
	
	public void setNombreCampana(String nombreCampana) {
		this.nombreCampana = nombreCampana;
	}
	
	public Integer getCodigoCampana() { return this.codigoCampana; }
	public Integer getCodigoTipo() { return this.codigoTipo; }
	
	
}
